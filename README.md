# Online Learning SDK for IoT-NGIN

## Backend frameworks

The sdk encapsulates three different frameworks that offer Incremental Learning algorithms:

- [Vowpal Wabbit](https://vowpalwabbit.org/): fast, flexible, online, and active learning solution.
- [Scikit Learn](https://scikit-learn.org/): Simple and efficient tools for predictive data analysis.
- [Tensorflow](https://www.tensorflow.org/): An end-to-end open source machine learning platform. 
- [Pytorch](https://pytorch.org/): An open source machine laerning framework.

The SDK comes with a default configuration for each of the frameworks. However, this configuration can be adjusted. To know more about how to configure each framework and how to choose between different algorithms and parameters, refer to the [Online Learning Readme file](./src/README.md). Some of those parameters can be directly configured upon launch as explained in the Serving section. 

## Serving as a REST API

The service is deployed using [Kserve](https://kserve.github.io/website/0.7/) The python script [`create_onlinelearning_service_kubeflow.py`](./create_online_learning_service_kubeflow.py) launches a web server with the OL module running as HTTP REST API. The configuration parameters are shown below:

- "--backend" Default='pytorch'. Online learning backend framework. Options: tensorflow, sklearn, vowpal_wabbit, pytorch.
- "--inference_mode": Default='regression'. Whether it is a regression or classification task.
- "--classes": Default='None'. List of classes for classification tasks. Examples on format: "('class_1', 'class_2')" or "(1,0)". __Remember to use `"` before and after the parenthesis__.
- "--minio_bucket": Default='None'. MinIO bucket where save the model.
- "--minio_model_name": Default='None'. Model name saved in MinIO.
- "--minio_scaler_name": Default='None'. Scaler name saved in MinIO.
- "--local_model_name": Default='None'. Model name to save locally.
- "--ol_service_name": Default='None'. Online laerning service name.
- "--connector_type": Default='None'. Techology for collecting data. Options: mqtt, kafka, rest, hybrid.
- "--topic_predictions": Default='None'. Topic where predictions are published.

Moreover, credentials are required so that the ML models can be upload/download from MinIO. These credentials must be defined as environment variables in a __.env__ file. The variables are:

- MINIO_SECRET_KEY=< PASSWORD >
- MINIO_ACCESS_KEY=< MINIO_USER >
- MINIO_HOST=< MINIO_HOST >

Example of launch:

```
$ python3 create_onlinelearning_service.py --backend sklearn --inference_mode classification --classes "('cat', 'dog')"
```

The service opens a single endpoint which is waiting for model update or inferences requests via `POST` method. The endpoint receives a `JSON` object which __must__ contain at least the inputs to perform predictions. If the `JSON` object contain also the outputs, the service will perform an inference. 
The endpoint presents the following format:

https://$InferenceServiceName.$Domain.$Extension/v1/models/$ModelName:predict

> Notice that the framework supports multiclass classification but not multivariate classification, neither multivariate regression. This means that outputs can only be one value per sample.

### Examples of usage:

To update the model, the request must contain both inputs and outputs:

```
curl --location --request POST 'http://localhost:8080/v1/models/ol-smart-grid-power-consumption:predict' \
--header 'Content-Type: application/json' \
--data-raw '{
    "input": {
        "a1": 1,
        "a2": 2,
        "a3": "tall"
    },
    "output": {
        "o1": "cat"
    }
}'
```

```
curl --location --request POST 'http://localhost:8080/v1/models/ol-smart-grid-power-consumption:predict' \
--header 'Content-Type: application/json' \
--data-raw '{
    "input": [15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 9.0, 8.0, 11.0, 12.0],
    "output": [15.0]
}'
```

To ask for an inference, the request must contain the inputs:

```
curl --location --request POST 'http://localhost:8080/v1/models/ol-smart-grid-power-consumption:predict' \
--header 'Content-Type: application/json' \
--data-raw '{
    "a1": 2,
    "a2": 1,
    "a3": "short"
}'
```

```
curl --location --request POST 'http://localhost:8080/v1/models/ol-smart-grid-power-consumption:predict' \
--header 'Content-Type: application/json' \
--data-raw '[15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 9.0, 8.0, 11.0, 12.0]'
```

## Stream Data Connectors

The Online Learning service must support data coming from Kafka/MQTT brokers. Kserve framework only provides HTTP support, therefore, a [Camel-K](https://camel.apache.org/camel-k/1.10.x/index.html) integration is needed. The integration is created using [camel-k-binding.yaml](./kubeflow/camel-k-binding.yaml). This file must define the following attributes and credentials:
- Broker URL.
- Topic where data is been dumped.
- Client id.
- Username.
- Password.
- OL service endpoint deployed by Kserve.

The OL service must publish in the topic
Just like MinIO. Streaming data support needs credentials so that the OL service can send data to the broker. These credentials must be defined in the __.env__ file, as done with MinIO. The variables are:

- CONNECTOR_TYPE=< CONNECTOR_TYPE >
- BROKER_URL=< URL >
- BROKER_PORT=< PORT >
- BROKER_USER=< USER_ID >
- BROKER_KEY=< PASSWORD >

Once credentials are defined, el service is able to connect to the broker. The predictions must be dumped into the specific topic. Thus, a configuration argument is needed to determine the topics where the inference results should be dumped.

- "--predictions": Default='predictions'. Topic where the model sends the predictions obtained from'to_predict' topic data.


## Deploy Service using Kubeflow

El despliegue del servicio de OL requiere una serie require los siguientes pasos:

1. OL service adaptation. This step involves the definition of all configuration parameters.
2. Create the Docker image. Once the OL is adapted to the specific use case, it is time to create the Docker image and upload it to a Docker registry. 
3. Define Kserve YAML manifest. Once Docker image is ready to be pulled. The Kserve manifest must be defined so that the inference service could be deployed. [kserve_isvc](./kubeflow/kserve_isvc.yaml) contains the template to create the manifest.
4. Create and run Kubeflow pipeline. This step defines the whole pipeline (in this case it contains only the deployment service component)
5. Define Camel-k integration (OPTIONAL). As explained in previous section, the camel-k integration is needed when data comes from MQTT/Kafka flows. Here is the template [camel-k-binding.yaml](./kubeflow/camel-k-binding.yaml).