""" Explainer component powered by Kserve Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.
import logging
import os
from typing import Dict
import kserve
import torch
import numpy as np
from dotenv import load_dotenv
from captum.attr import DeepLift
from ..utils.ml.load_save_model import ModelLoader
from ..utils.data.minio_client import MinioConnector

load_dotenv()

# Receives data from the Transformer maybe it requires also the prediction
# computes some kind of explanation or feature importance

KSERVE_SERVER_HOST=os.getenv('KSERVE_SERVER_HOST')
KSERVE_MODEL_NAME=os.getenv('KSERVE_MODEL_NAME')
KSERVE_TRANSFORMER_PORT=os.getenv('KSERVE_TRANSFORMER_PORT')
MINIO_USER = os.getenv("MINIO_ACCESS_KEY")
MINIO_PASS = os.getenv('MINIO_SECRET_KEY')
MINIO_HOST = os.getenv('MINIO_HOST')
MINIO_BUCKET = os.getenv('MINIO_BUCKET')
MINIO_SCALER_NAME = os.getenv('MINIO_SCALER_NAME')
MINIO_MODEL_NAME = os.getenv("MINIO_MODEL_NAME")
MINIO_BACKEND_MODULE = os.getenv("MINIO_BACKEND_MODULE")

STREAMING_CONNECTOR_TYPE = os.getenv('STREAMING_CONNECTOR_TYPE')
BROKER_URL = os.getenv('BROKER_URL')
BROKER_PORT = os.getenv('BROKER_PORT')
BROKER_USER = os.getenv('BROKER_USER')
BROKER_KEY = os.getenv('BROKER_KEY')
PREDICTIONS_TOPIC = os.getenv('PREDICTIONS_TOPIC')

LOCAL_MODEL_NAME = os.getenv("LOCAL_MODEL_NAME")
LOCAL_BACKEND_MODULE_NAME = os.getenv("LOCAL_BACKEND_MODULE_NAME")
DYNAMIC_IMPORT_PATH = os.getenv("DYNAMIC_IMPORT_PATH")

BACKEND = os.getenv("BACKEND")

KSERVE_EXPLAINER_PORT=os.getenv('KSERVE_EXPLAINER_PORT')

MINIO_CONNECTOR = MinioConnector(
                            MINIO_USER,
                            MINIO_PASS,
                            MINIO_HOST)

class Explainer(kserve.KFModel):
    """Explainer Kserve Class"""
    def __init__(self, name: str):
        """
        Constructor
            Parameters
                Inputs:
                    - name: Model name
        """
        super().__init__(name)
        self.model_name = name
        self.predictor_model = None
        self.scaler = None
        self.minio_client = None
        self.scaler_loader = None
        self.model_bucket = None
        self.local_scaler_name = './scaler'
        self.inference_request_flag = False
        self.publisher_client = None
        self.last_prediction=None
        self.model_loader=None
        self.set_up()

    def set_up(self):
        """ Set up function """
        self.import_module_backend()
        return 0

    def load(self):
        """ Loads model from MinIO """
        self.model_loader = ModelLoader(
                                    local_model_name=LOCAL_MODEL_NAME,
                                    backend=BACKEND,
                                    module_name=DYNAMIC_IMPORT_PATH
                                    )
        minio_client = MINIO_CONNECTOR.get_client()
        minio_client.fget_object(MINIO_BUCKET, MINIO_MODEL_NAME, LOCAL_MODEL_NAME)
        self.predictor_model = self.model_loader.load_model()
        self.ready = True

    def import_module_backend(self):
        """ Loads module script from MinIO """
        if MINIO_BACKEND_MODULE is not None:
            minio_client = MINIO_CONNECTOR.get_client()
            minio_client.fget_object(MINIO_BUCKET,
                                    MINIO_BACKEND_MODULE,
                                    LOCAL_BACKEND_MODULE_NAME)

    def get_deep_lift_attributes(self, x_test):
        """
        Function to compute feature importance using DeepLIFT
            Parameters
                Inputs:
                    - x_test: Input features
        """
        self.load()
        dl_explainer = DeepLift(self.predictor_model)
        dl_attr_test = dl_explainer.attribute(x_test)
        dl_attr_test_sum = dl_attr_test.detach().numpy().sum(0)
        dl_attr_test_norm_sum = dl_attr_test_sum / np.linalg.norm(dl_attr_test_sum, ord=1)
        return dl_attr_test_norm_sum.tolist()

    def explain(self, request: Dict) -> Dict:
        """
        HTTP endpoint function. It receives the input features to capture
        the feature importance in the prediction
            Parameters
                Inputs:
                    - request: HTTP request with input features
        """
        input_features = request["inputs"]
        input_features = torch.tensor(input_features).reshape(1,36,1)
        dl_explanations = self.get_deep_lift_attributes(input_features)
        return {"deep_lift_explanations": dl_explanations}

if __name__ == "__main__":
    logging.info("Starting explainer")
    logging.info("Predictor Host: %s", KSERVE_SERVER_HOST)
    logging.info("Model Name: %s", KSERVE_MODEL_NAME)
    explainer = Explainer(KSERVE_MODEL_NAME)
    kserve.KFServer(http_port=KSERVE_EXPLAINER_PORT).start(models=[explainer])
