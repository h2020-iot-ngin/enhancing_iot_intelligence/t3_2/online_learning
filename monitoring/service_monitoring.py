import os
import yaml
from fastapi import FastAPI, Request, BackgroundTasks
from fastapi.responses import FileResponse
import uvicorn
import pandas as pd
from evidently.metric_preset import DataDriftPreset
from evidently.report import Report
from evidently.test_suite import TestSuite
from evidently.test_preset import DataStabilityTestPreset
from evidently_real_time.monitoring import (
    getMonitoringService,
    MonitoringService,
)
from starlette_exporter import PrometheusMiddleware, handle_metrics

app = FastAPI()
app.add_middleware(
    PrometheusMiddleware,
    app_name="sample_fast_api",
    prefix="sample_fast_api",
)
app.add_route("/metrics", handle_metrics)

# DATA_PATH = "/home/jorge_mira/projects/FMLE/fmle_example/synthetic_data_with_target.csv"
REF_DATA = "/home/jorge_mira/projects/IoT-NGIN/online_learning/monitoring/ref_data.csv"
CURRENT_DATA = "/home/jorge_mira/projects/IoT-NGIN/online_learning/monitoring/current_data.csv"

def load_data():
    reference_data = pd.read_csv(REF_DATA, index_col=0)
    current_data = pd.read_csv(CURRENT_DATA, index_col=0)
    # ref_data = pd.DataFrame()
    # ref_data["power"] = reference_data["power"]
    cur_data = pd.DataFrame()
    cur_data["power"] = current_data["power"]
    # reference_data = data.loc[:300]
    # current_data = data.iloc[0:2]
    # ref_data.to_csv("./ref_data.csv")
    cur_data.to_csv("./current_data.csv")
    # print(current_data.head())
    output = {
        "current_data":current_data,
        "reference_data":reference_data
        }
    return output

@app.get("/")
def hello_world():
    """Hello world Endpoint"""
    load_data()
    return "Welcome to Data Monitoring Service"

# @app.get("/store-current-data")
def store_data(new_data):
    # inputs = request['instances']
    # json_data = await request.json()
    # print(json_data)
    # new_data = pd.DataFrame(json_data)
    print(new_data.head())
    data = pd.read_csv("./current_data.csv", index_col=0)
    print(data.head())
    data = pd.concat([data,new_data], ignore_index=True)
    print(data.head())
    data.to_csv("./current_data.csv")


@app.get("/data-drift")
def data_drift():
    """Data drift Endpoint"""
    # data = load_data()
    current_data = pd.read_csv("./current_data.csv", index_col=0)
    reference_data = pd.read_csv("./ref_data.csv", index_col=0)
    data_drift_report = Report(metrics=[DataDriftPreset()])

    data_drift_report.run(current_data=current_data, reference_data=reference_data, column_mapping=None)
    data_drift_report.save_html("data-drift-report.html")
    return FileResponse("data-drift-report.html")
    # return "Welcome to Data Drift Endpoint"
    # return f"{current_data.describe()}"

@app.get("/data-stability-test")
def data_stability():
    # data = load_data()
    current_data = pd.read_csv("./current_data.csv", index_col=0)
    reference_data = pd.read_csv("./ref_data.csv", index_col=0)
    data_stability= TestSuite(tests=[
        DataStabilityTestPreset(),
        ])
    data_stability.run(current_data=current_data, reference_data=reference_data, column_mapping=None)
    data_stability.save_html("data-stability-report.html")
    return FileResponse("data-stability-report.html")

@app.on_event("startup")
async def startup_event():
    global SERVICE
    config_file_name = "service_config.yaml"
    # try to find a config file, it should be generated via a data preparation script
    if not os.path.exists(config_file_name):
        exit(
            "Cannot find config file for the metrics service. Try to check README.md for setup instructions."
        )

    with open(config_file_name, "rb") as config_file:
        config = yaml.safe_load(config_file)

    SERVICE = getMonitoringService(config)

@app.post("/monitor/{dataset}")
async def monitoring(dataset:str, request:Request, background_tasks: BackgroundTasks):
    json_data = await request.json()
    data = pd.DataFrame(json_data)
    # store_data(data)
    print(data)
    print(data.info())
    background_tasks.add_task(SERVICE.iterate, dataset_name=dataset, new_rows=data)


if __name__ == "__main__":
    uvicorn.run(app, port = 5100)
