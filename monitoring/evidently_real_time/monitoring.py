from typing import Dict, List, Optional
from evidently import model_monitoring
from evidently.pipeline.column_mapping import ColumnMapping
from evidently.model_monitoring import DataDriftMonitor, RegressionPerformanceMonitor, ModelMonitoring
from evidently.runner.loader import DataLoader, DataOptions
import hashlib
import pandas
import datetime
import dataclasses
import logging
from prometheus_client import Gauge
import pandas as pd
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def getMonitoringService(config):
    loader = DataLoader()
    logger.info(f"config: {config}")
    options = MonitoringServiceOptions(**config["service"])

    # Load the data --------------------------------------------------------
    # Maybe it is interesting to make this easier.
    # options = MonitoringServiceOptions(**config["service"])
    datasets_path = os.path.abspath(options.datasets_path)
    loader = DataLoader()

    datasets = {}

    for dataset_name in os.listdir(datasets_path):
        logging.info(f"Load reference data for dataset %s", dataset_name)
        reference_path = os.path.join(datasets_path, dataset_name,"reference_data.csv")
        if dataset_name in config["datasets"]:
            dataset_config = config["datasets"][dataset_name]
            reference_data = loader.load(
                reference_path,
                DataOptions(
                    date_column=dataset_config.get("date_column", None),
                    separator=dataset_config["data_format"]["separator"],
                    header=dataset_config["data_format"]["header"],
                ),
            )
            datasets[dataset_name] = LoadedDataset(
                name=dataset_name,
                references=reference_data,
                monitors=dataset_config["monitors"],
                column_mapping=ColumnMapping(**dataset_config["column_mapping"]),
            )
            logging.info("Reference is loaded for dataset %s: %s rows", dataset_name, len(reference_data))
        else:
            logging.info("Dataset %s is not configured in the config file", dataset_name)
    svc = MonitoringService(datasets=datasets, window_size=options.window_size)
    return svc


@dataclasses.dataclass
class MonitoringServiceOptions:
    datasets_path: str
    min_reference_size: int
    use_reference: bool
    moving_reference: bool
    window_size: int
    calculation_period_sec: int


@dataclasses.dataclass
class LoadedDataset:
    name: str
    references: pd.DataFrame
    monitors: List[str]
    column_mapping: ColumnMapping

monitor_mapping = {"data_drift": DataDriftMonitor,
                   "regression_performance": RegressionPerformanceMonitor}


class MonitoringService:
    # names of monitoring datasets
    datasets: List[str]
    metric: Dict[str, Gauge]
    last_run: Optional[datetime.datetime]
    # collection of reference data
    reference: Dict[str, pd.DataFrame]
    # collection of current data
    current: Dict[str, Optional[pd.DataFrame]]
    # collection of monitoring objects
    monitoring: Dict[str, ModelMonitoring]
    calculation_period_sec: float = 15
    window_size: int

    def __init__(self, datasets: Dict[str, LoadedDataset], window_size: int):
        self.reference = {}
        self.monitoring = {}
        self.current = {}
        self.column_mapping = {}
        self.window_size = window_size

        for dataset_info in datasets.values():
            self.reference[dataset_info.name] = dataset_info.references
            self.monitoring[dataset_info.name] = ModelMonitoring(
                monitors=[monitor_mapping[k]() for k in dataset_info.monitors], options=[]
            )
            self.column_mapping[dataset_info.name] = dataset_info.column_mapping

        self.metrics = {}
        self.next_run_time = {}
        self.hash = hashlib.sha256(pd.util.hash_pandas_object(self.reference["data_drift_dataset"]).values).hexdigest()
        self.hash_metric = Gauge("evidently:reference_dataset_hash", "", labelnames=["hash"])

    def iterate(self, dataset_name: str, new_rows: pd.DataFrame):
        """Add data to current dataset for specified dataset"""
        window_size = self.window_size
        

        if dataset_name in self.current:
            current_data = self.current[dataset_name].append(new_rows, ignore_index=True)

        else:
            current_data = new_rows

        current_size = current_data.shape[0]
        current_data.reset_index(inplace=True, drop=True)
        # if current_size > self.window_size:
        if self.window_size < current_size:
            # cut current_size by window size value
            current_data.drop(index=list(range(0, current_size - self.window_size)), inplace=True)
            current_data.reset_index(drop=True, inplace=True)

        self.current[dataset_name] = current_data

        if current_size < window_size:
            logging.info(f"Not enough data for measurement: {current_size} of {window_size}." f" Waiting more data")
            return

        next_run_time = self.next_run_time.get(dataset_name)

        if next_run_time is not None and next_run_time > datetime.datetime.now():
            logging.info("Next run for dataset %s at %s", dataset_name, next_run_time)
            return

        self.next_run_time[dataset_name] = datetime.datetime.now() + datetime.timedelta(
            seconds=self.calculation_period_sec
        )
        self.monitoring[dataset_name].execute(
            self.reference[dataset_name], current_data, self.column_mapping[dataset_name]
        )
        self.hash_metric.labels(hash=self.hash).set(1)

        for metric, value, labels in self.monitoring[dataset_name].metrics():
            metric_key = f"evidently:{metric.name}"
            found = self.metrics.get(metric_key)

            if not labels:
                labels = {}

            labels["dataset_name"] = dataset_name

            if isinstance(value, str):
                continue

            if found is None:
                found = Gauge(metric_key, "", list(sorted(labels.keys())))
                self.metrics[metric_key] = found

            try:
                found.labels(**labels).set(value)

            except ValueError as error:
                # ignore errors sending other metrics
                logging.error("Value error for metric %s, error: ", metric_key, error)