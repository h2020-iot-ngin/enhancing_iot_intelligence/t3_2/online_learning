import requests
import numpy as np
import time
import pandas as pd

CURRENT_DATA = "/home/jorge_mira/projects/IoT-NGIN/online_learning/monitoring/current_data.csv"

data = pd.read_csv(CURRENT_DATA, index_col=0)

print(data.shape)

inputs = []

for i in range(36):
    inputs.append(data["power"].iloc[i])
for i in range(36,len(data)):
    print(f"New request {inputs}")
    request_dict = {
        "inputs":inputs,
        "type": "float",
        "input_shape":[1, 36, 1]
    }
    requests.post(
                'http://localhost:8080/v1/models/ol-smart-grid-power-consumption:predict',
                json=request_dict,
                headers={'Content-Type': 'application/json'},
                timeout=10)
    inputs.append(data["power"].iloc[i])
    inputs.pop(0)
    time.sleep(8)


# MAX_ITER= 20

# for i in range(MAX_ITER):
#     print(f"Iteration {i+1} out of {MAX_ITER} ")
#     request_dict = {
#         "entity_id":["entity"],
#         "mean_acc":[0.018+ np.random.normal(0, 0.023,1)[0]],
#         "mean_fuel":[0.12+np.random.normal(0,0.05,1)[0]],
#         "mean_speed":[40.0+np.random.normal(0,20,1)[0]],
#         "duration":[3541.0+np.random.normal(0,500,1)[0]],
#         "route_length":[8.4+np.random.normal(0,3,1)[0]]
#     }
#     requests.post(
#                 'http://localhost:8080/v1/models/demo-inference-service:predict',
#                 json=request_dict,
#                 headers={'Content-Type': 'application/json'},
#                 timeout=10)
#     time.sleep(4)