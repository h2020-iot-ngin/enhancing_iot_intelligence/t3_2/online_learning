# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

WORKDIR /online_learning

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt
COPY . /online_learning

ENTRYPOINT ["python", "serving/create_onlinelearning_service.py"]

# FROM python:3.8

# # Keeps Python from generating .pyc files in the container
# ENV PYTHONDONTWRITEBYTECODE=1

# # Turns off buffering for easier container logging
# ENV PYTHONUNBUFFERED=1

# WORKDIR /online_learning

# # Install pip requirements
# COPY requirements.txt .
# RUN python -m pip install -r requirements.txt
# COPY kafka_test_admin_client.py kafka_test_admin_client.py

# ENTRYPOINT ["python", "kafka_test_admin_client.py"]