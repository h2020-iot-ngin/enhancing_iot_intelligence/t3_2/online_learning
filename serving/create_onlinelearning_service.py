""" Online Learning Service Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import argparse
import sys
import os
import ast
import json
import time as time_lib
import flask
from flask_cors import CORS
# TODO When creating the pypi project, this must be updated with the name of the library
# instead of paths

parent_path = os.getcwd().split("/")[:-1]
CURRENT_PATH = '/'.join(parent_path)
sys.path.append(CURRENT_PATH)

from utils.data.streams_connector import Streamer
from src.online_learning import OnlineLearning, BackendType


app = flask.Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/update", methods=["POST"])
def update():
    """
        Perform an update on the model.

        Parameters
        ----------
        json_array : Json object with two keys: inputs and outputs. The keys can contain a list
        to enable batch updates.
    """
    # global OL_SERVICE

    request = flask.request
    results = {"status": "error"}

    if request.method == "POST":
        results["status"] = "success"

        stat = 200

        start_time = time_lib.process_time()

        json_array = request.get_json()

        inputs = json_array["input"]
        outputs = json_array["output"]

        error = OL_SERVICE.update(inputs, outputs)

        if error:
            results["status"] = "internal_server_error"
            stat = 500

        end_time = time_lib.process_time()

        elapsed_time = end_time - start_time
        results["latency"] = elapsed_time
    else:
        results["status"] = "error_method_incorrect"
        stat = 405

    return flask.jsonify(results), stat


@app.route("/predict", methods=["POST", "GET"])
def predict():
    """
        Return a prediction based on the input data.

        Parameters
        ----------
        X : input data in json format
    """
    # global OL_SERVICE

    request = flask.request
    results = {"status": "error"}

    if request.method in ['POST', 'GET']:#request.method == "POST" or request.method == "GET":
        results["status"] = "success"
        stat = 200
        start_time = time_lib.process_time()

        json_array = request.get_json()
        input_data = json_array['input']
        predictions = OL_SERVICE.predict(input_data)

        if predictions is None:
            results["status"] = "internal_server_error"
            stat = 500
        else:
            results["predictions"] = predictions

        end_time = time_lib.process_time()

        elapsed_time = end_time - start_time
        results["latency"] = elapsed_time
    else:
        results["status"] = "error_method_incorrect"
        stat = 405

    return flask.jsonify(results), stat


@app.route("/info", methods=["GET"])
def info():
    """
        Return information about the deployment.
    """

    request = flask.request
    results = {"status": "error"}

    if request.method == "GET":
        results["status"] = "success"

        stat = 200

        start_time = time_lib.process_time()

        metrics = OL_SERVICE.info()
        for key in metrics:
            results[key] = OL_SERVICE.metrics[key]

        end_time = time_lib.process_time()

        elapsed_time = end_time - start_time
        results["latency"] = elapsed_time
    else:
        results["status"] = "error_method_incorrect"
        stat = 405

    return flask.jsonify(results), stat

def set_up_():
    """
        Function to set up the service structure using the argument variables.
    """
    set_up_output = {
            'rest_connector':None,
            'mqtt_connector':None,
            'kafka_connector':None,
            'mqtt_consumer_client':None,
            'mqtt_publisher_client':None,
            'kafka_consumer_client':None,
            'kafka_publisher_client':None,
    }

    if ARGS.connector_type == 'kafka':
        # server_address = ARGS.broker_address +':' +str(ARGS.broker_port_kafka)
        connector = Streamer(Streamer.ConnectorTypes.KAFKA).connector
        consumer_client = connector.Consumer(
                                            topic_names=[
                                                        ARGS.topic_to_predict,
                                                        ARGS.topic_to_update],
                                            broker_address=ARGS.broker_address,
                                            port= ARGS.broker_port_kafka)

        publisher_client = connector.Publisher(
                                            broker_address=ARGS.broker_address,
                                            port= ARGS.broker_port_kafka)
        # _on_message_client_kafka()
        set_up_output['kafka_connector'] = connector
        set_up_output['kafka_consumer_client'] = consumer_client
        set_up_output['kafka_publisher_client'] = publisher_client
    elif ARGS.connector_type == 'mqtt':
        connector = Streamer(Streamer.ConnectorTypes.MQTT).connector
        connector.Consumer.on_message = _on_message_client_mqtt
        publisher_client = connector.Publisher(
            broker_address=ARGS.broker_address,
            port=ARGS.broker_port_mqtt
            )
        consumer_client = connector.Consumer(
            topic_names = [
                ARGS.topic_to_update,
                ARGS.topic_to_predict],
            broker_address = ARGS.broker_address,
            port = ARGS.broker_port_mqtt
            )
        set_up_output['mqtt_connector'] = connector
        set_up_output['mqtt_consumer_client'] = consumer_client
        set_up_output['mqtt_publisher_client'] = publisher_client
    elif ARGS.connector_type == 'hybrid':
        # Kafka
        kafka_connector = Streamer(Streamer.ConnectorTypes.KAFKA).connector
        kafka_consumer_client = kafka_connector.Consumer(
            topic_names=[
                ARGS.topic_to_predict,
                ARGS.topic_to_update],
                broker_address=ARGS.broker_address,
                port = ARGS.broker_port_kafka)
        kafka_publisher_client = kafka_connector.Publisher(
            broker_address=ARGS.broker_address,
            port = ARGS.broker_port_kafka)

        # MQTT
        mqtt_connector = Streamer(Streamer.ConnectorTypes.MQTT).connector
        mqtt_consumer_client = mqtt_connector.Consumer(
            topic_names=ARGS.topic_predictions,
            broker_address=ARGS.broker_address,
            port=ARGS.broker_port_mqtt)
        mqtt_publisher_client = mqtt_connector.Publisher(
            broker_address=ARGS.broker_address,
            port=ARGS.broker_port_mqtt)

        set_up_output['kafka_connector'] = kafka_connector
        set_up_output['kafka_consumer_client'] = kafka_consumer_client
        set_up_output['kafka_publisher_client'] = kafka_publisher_client
        set_up_output['mqtt_connector'] = mqtt_connector
        set_up_output['mqtt_consumer_client'] = mqtt_consumer_client
        set_up_output['mqtt_publisher_client'] = mqtt_publisher_client

    elif ARGS.connector_type == 'rest':
        print('REST')
        connector = 'rest'
        set_up_output['rest_connector'] = connector
        # return connector
    else:
        print('ERROR: connector type not understood. Options: mqtt, kafka, hybrid or rest')
        sys.exit(1)
    return set_up_output

def _on_message_client_mqtt(self, client, userdata, message):
    self.messages.append(message.payload.decode("utf-8"))
    message = self.consume_one()
    message = json.loads(message)
    if message["topic"] == "to_update":
        print('Updating model')
        value = message["message"]
        inputs = value['input']
        outputs = value['output']
        error = OL_SERVICE.update(inputs, outputs)
        if error != 0:
            print("internal_server_error")
    elif message["topic"] == "to_predict":
        value = message["message"]
        inputs = value["input"]
        print('Performing prediction')
        predictions = OL_SERVICE.predict(inputs)
        if predictions is None:
            print("internal_server_error")
        else:
            msg_2 ={"topic":'predictions',
                    "input": inputs,
                    "prediction": predictions}
            PUBLISHER_CLIENT.publish_one('predictions', value=json.dumps(msg_2).encode('utf-8'))

def _on_message_client_kafka():
    time_lib.sleep(10)
    print(f"Listening {CONSUMER_CLIENT.consumer.topics()} topics.")
    for message in CONSUMER_CLIENT.consumer:
        if message.topic == "to_update":
            print('Updating model')
            inputs = message.value['input']
            outputs = message.value['output']
            error = OL_SERVICE.update(inputs, outputs)
            if error != 0:
                print("internal_server_error")
        elif message.topic == "to_predict":
            print('Performing a prediction')
            inputs = message.value['input']
            predictions = OL_SERVICE.predict(inputs)
            if predictions is None:
                print("internal_server_error")
            msg_2 ={"topic":"predictions",
                    "input": inputs,
                    "predictions": predictions}
            PUBLISHER_CLIENT.publish_one('predictions', value=msg_2)

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("--app_port", type=str, default='5100',
                        help="application port", required=False)
    PARSER.add_argument("--backend", type=str, default='vowpal_wabbit',
                        help="""online learning backend framework. Options:
                        tensorflow, sklearn, vowpal_wabbit""",
                        required=False)
    PARSER.add_argument("--version", type=str, default='1.0',
                        help="model version", required=False)
    PARSER.add_argument("--inference_mode", type=str, default="classification",
                        help="regression or classification task", required=False)
    PARSER.add_argument("--classes", type=str, default="('dog', 'cat')",
                        help="list of classes for classification", required=False)
    PARSER.add_argument("--topic_predictions", type=str, default="predictions",
                        required=False, help="topic where predictions are published")
    PARSER.add_argument("--topic_to_predict", type=str, default="to_predict",
                        required=False, help="topic where data for predictions is published")
    PARSER.add_argument("--topic_to_update", type=str, default='to_update',
                        help="topic for update the model", required=False)
    PARSER.add_argument("--connector_type", type=str, default="mqtt",
                        help="Techology for collecting data. Options: mqtt, kafka, rest, hybrid",
                        required=False)
    PARSER.add_argument("--broker_port_mqtt", type=int, default=1883,
                        help="Broker port for mqtt", required=False)
    PARSER.add_argument("--broker_port_kafka", type=int, default=19092,
                        help="Broker port for kafka", required=False)
    PARSER.add_argument("--broker_address", type=str, default="127.0.0.1",
                        help="broker ip address in case kafka or mqtt", required=False)

    ARGS, UNKNOWN = PARSER.parse_known_args()

    classes = ast.literal_eval(ARGS.classes)

    if ARGS.backend.lower() == BackendType.TENSORFLOW.value:
        OL_SERVICE = OnlineLearning(
            BackendType.TENSORFLOW,
            *UNKNOWN,
            inference_mode=ARGS.inference_mode,
            classes=classes)

        # set_up()

    elif ARGS.backend.lower() == BackendType.SKLEARN.value:
        OL_SERVICE = OnlineLearning(
            BackendType.SKLEARN,
            *UNKNOWN,
            inference_mode=ARGS.inference_mode,
            classes=classes)

        # set_up()
    elif ARGS.backend.lower() == BackendType.VOWPAL_WABBIT.value:
        OL_SERVICE = OnlineLearning(
            BackendType.VOWPAL_WABBIT,
            *UNKNOWN,
            inference_mode=ARGS.inference_mode,
            classes=classes)

        # set_up()
    else:
        print("ERROR: Backend type not understood. Options: tensorflow, sklearn, vowpal_wabbit")
        sys.exit(1)

    set_up_variables = set_up_()

    if ARGS.connector_type == 'mqtt':
        CONSUMER_CLIENT = set_up_variables['mqtt_consumer_client']
        PUBLISHER_CLIENT = set_up_variables['mqtt_publisher_client']
        CONSUMER_CLIENT.consumer.loop_forever()
    elif ARGS.connector_type == 'kafka':
        CONSUMER_CLIENT = set_up_variables['kafka_consumer_client']
        PUBLISHER_CLIENT = set_up_variables['kafka_publisher_client']
        _on_message_client_kafka()
    else:
        CONNECTOR = set_up_variables['rest_connector']

    app.run(host="0.0.0.0", port=ARGS.app_port, debug=False, threaded=True)
