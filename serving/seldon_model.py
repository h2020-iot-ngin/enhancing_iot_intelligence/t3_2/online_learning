"""Seldon Model Serving Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

class SeldonModel:
    """Seldon Model Serving"""
    def __init__(self):
        """
            Add any initialization parameters. These will be passed at runtime from the graph
            definition parameters defined in your seldondeployment kubernetes resource manifest.
        """
        print("Initialising")

    #pylint: disable=no-self-use
    # pylint: disable=unused-argument
    # Some pylint warnings are disabled since the class is not completely implemented
    def predict(self, array_input, features_names):
        """
            Return a prediction.

            Parameters
            ----------
            array_input : array-like
            feature_names : array of feature names (optional)
        """
        print("Predict called")
        return array_input

    def metrics(self):
        """
            Return metrics about the deployment.
        """
        raise NotImplementedError

    # def transform_input(self, X: np.ndarray, names: Iterable[str],
    #                     meta: dict = None) -> Union[np.ndarray, list, str, bytes]:
    #     raise NotImplementedError

    # def transform_output(self, X: np.ndarray, names: Iterable[str],
    #                     meta: dict = None) -> Union[np.ndarray, list, str, bytes]:
    #     raise NotImplementedError
