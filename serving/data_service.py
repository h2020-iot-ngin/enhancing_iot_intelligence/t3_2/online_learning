#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

""" Data Service Module """

import time as time_lib
import argparse
import os
import sys
import json
import flask
from flask_cors import CORS

parent_path = os.getcwd().split("/")[:-1]
CURRENT_PATH = '/'.join(parent_path)
sys.path.append(CURRENT_PATH)

from utils.data.streams_connector import Streamer

app = flask.Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route("/publish", methods=["POST"])
def publish():
    """
        Perform publication to a topic.

        Parameters
        ----------
        json_array : Json object with 2 keys: topic and message.
    """

    request = flask.request
    results = {"status": "error"}

    if request.method == "POST":
        results["status"] = "success"

        stat = 200

        start_time = time_lib.process_time()

        json_array = request.get_json()
        topic = json_array["topic"]
        msg = json_array["message"]

        if ARGS.connector_type == "mqtt":
            print(json_array)
            publisher.publish_one(topic_name=topic, value=json.dumps(json_array).encode('utf-8'))
        else:
            print(topic)
            print(msg)
            publisher.publish_one(topic_name=topic, value=msg)

        end_time = time_lib.process_time()
        elapsed_time = end_time - start_time
        results["latency"] = elapsed_time
    else:
        stat = 404
        results["status"] = "error_method_incorrect"


    return flask.jsonify(results), stat


@app.route("/consume", methods=["GET"])
def consume():
    """
        Perform consumption of one message in a topic.

        Parameters
        ----------
    """
    request = flask.request
    results = {"status": "error"}

    if request.method == "GET":
        results["status"] = "success"

        stat = 200

        start_time = time_lib.process_time()

        msg = consumer.consume_one()
        print(msg)
        results['payload'] = msg

        end_time = time_lib.process_time()

        elapsed_time = end_time - start_time
        results["latency"] = elapsed_time
    else:
        stat = 404
        results["status"] = "error_method_incorrect"
    return flask.jsonify(results), stat

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("--app_port", type=str, default='5101',
                        help="Application port", required=False)
    PARSER.add_argument("--broker_port", type=int, default='19092',
                        help="broker port", required=False)
    PARSER.add_argument("--connector_type", type=str, default='kafka',
                        help="connector type", required=False)
    PARSER.add_argument("--broker_address", type=str, default='127.0.0.1',
                        help="broker address.", required=False)
    PARSER.add_argument("--topic", type=str, default='test',
                        help="topic when consumer mode", required=False)

    ARGS, UNKNOWN = PARSER.parse_known_args()


    if ARGS.connector_type.lower() == Streamer.ConnectorTypes.MQTT.value:
        connector = Streamer(Streamer.ConnectorTypes.MQTT).connector
        consumer = connector.Consumer(topic_names=[ARGS.topic],
                                      broker_address=ARGS.broker_address,
                                      port=ARGS.broker_port)

        publisher = connector.Publisher(broker_address=ARGS.broker_address,
                                        port=ARGS.broker_port)

        consumer.consumer.loop_start()

    elif ARGS.connector_type.lower() == Streamer.ConnectorTypes.KAFKA.value:
        connector = Streamer(Streamer.ConnectorTypes.KAFKA).connector
        consumer = connector.Consumer(topic_names=[ARGS.topic],
                                      broker_address=ARGS.broker_address,
                                      port= ARGS.broker_port)
        publisher = connector.Publisher(broker_address=ARGS.broker_address,
                                        port= ARGS.broker_port)
    else:
        print("ERROR: Connector type not understood. Options: mqtt, kafka.")
        sys.exit(1)

    app.run(host="0.0.0.0", port=ARGS.app_port, debug=False, threaded=True)
