""" Online Learning Service powered by Kserve Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import requests
import pandas as pd

import logging
import argparse
import os
import ast
from typing import Dict
import torch
import kserve
from dotenv import load_dotenv
import tensorflow as tf
from src.online_learning import OnlineLearning, BackendType
from utils.ml.load_save_model import ModelLoader, ModelSaver
from utils.data.minio_client import MinioConnector

logging.basicConfig(level=logging.INFO)

# Load environment variables
load_dotenv()
MINIO_USER = os.getenv("MINIO_ACCESS_KEY")
MINIO_PASS = os.getenv('MINIO_SECRET_KEY')
MINIO_HOST = os.getenv('MINIO_HOST')
MINIO_BACKEND_MODULE = os.getenv("MINIO_BACKEND_MODULE")
MINIO_MODEL_NAME = os.getenv("MINIO_MODEL_NAME")
LOCAL_BACKEND_MODULE_NAME = os.getenv("LOCAL_BACKEND_MODULE_NAME")
DYNAMIC_IMPORT_PATH = os.getenv("DYNAMIC_IMPORT_PATH")
MINIO_BUCKET = os.getenv('MINIO_BUCKET')
MONITORING = os.getenv("MONITORING")
MONITORING_HOST = os.getenv("MONITORING_HOST")
KSERVE_MODEL_NAME=os.getenv('KSERVE_MODEL_NAME')
RECURSIVE=os.getenv("RECURSIVE")
OBJECT_DETECTION_MODEL = os.getenv("OBJECT_DETECTION_MODEL")

# Create minio client connector
MINIO_CONNECTOR = MinioConnector(
                            MINIO_USER,
                            MINIO_PASS,
                            MINIO_HOST)


class KFServingModel(kserve.KFModel):
    """KFservinf/Kserve class"""
    scaler = None
    model_backend = None
    model_bucket = None
    minio_model_name = None
    minio_scaler_name = None
    local_model_name = None
    local_scaler_name = None
    preprocessing_tools = None
    model = None

    def __init__(self, name: str):
        """ Constructor
        Args:
            - Name of the service
        """
        super().__init__(name)
        self.name = name
        self.ready = False
        self.client_connectors_dict = None
        self.backend = None
        self.model_loader = None
        self.model_saver = None
        self.publisher_client = None
        self.predictions_topic = None
        self.last_prediction = None
        self.prediction_list = []

    def import_module_backend(self):
        """ Loads module script from MinIO """
        if MINIO_BACKEND_MODULE is not None:
            minio_client = MINIO_CONNECTOR.get_client()
            minio_client.fget_object(self.model_bucket,
                                    MINIO_BACKEND_MODULE,
                                    LOCAL_BACKEND_MODULE_NAME)

    def load(self):
        """ Loads model from MinIO """
        self.model_loader = ModelLoader(
                                    local_model_name=self.local_model_name,
                                    backend=self.backend,
                                    module_name=DYNAMIC_IMPORT_PATH,
                                    recursive=RECURSIVE
                                    )
        if RECURSIVE is None:
            MINIO_CONNECTOR.download_model(
                                    self.model_bucket,
                                    self.minio_model_name,
                                    self.local_model_name)
        else:
            MINIO_CONNECTOR.download_files(self.model_bucket, self.minio_model_name)
        self.model_backend = self.model_loader.load_model()
        if self.minio_scaler_name is not None:
            MINIO_CONNECTOR.download_model(
                                    self.model_bucket,
                                    self.minio_scaler_name,
                                    self.local_scaler_name)
            self.scaler = self.model_loader.load_scaler(self.local_scaler_name)
        self.ready = True

    def save_model(self):
        """ saves model in MinIO """
        minio_client = MINIO_CONNECTOR.get_client()
        self.model_saver = ModelSaver(
                                    local_model_name=self.local_model_name,
                                    backend=self.backend
                                    )
        self.model_saver.save_model(self.model_backend)
        logging.info("Saving model in Minio")
        file_stat = os.stat(self.local_model_name)
        with open(self.local_model_name, "rb") as file_to_store:
        # file_to_store = open(self.local_model_name, "rb")
            minio_client.put_object(
                            self.model_bucket, self.minio_model_name,
                            file_to_store,file_stat.st_size)

    def set_up(self):
        """AI backend initialization"""
        # self.model_bucket = ARGS.minio_bucket
        # self.minio_model_name = ARGS.minio_model_name
        self.model_bucket = MINIO_BUCKET
        self.minio_model_name = MINIO_MODEL_NAME
        self.minio_scaler_name = ARGS.minio_scaler_name
        self.local_model_name = MINIO_MODEL_NAME #ARGS.minio_model_name
        self.local_scaler_name = ARGS.local_scaler_name
        self.backend =  ARGS.backend.lower()
        self.import_module_backend()
        self.load()
        if self.backend == BackendType.SKLEARN.value:
            self.model = OnlineLearning(
                            BackendType.SKLEARN,
                            *UNKNOWN,
                            inference_mode=ARGS.inference_mode,
                            classes=ast.literal_eval(ARGS.classes))
        elif self.backend == BackendType.VOWPAL_WABBIT.value:
            self.model = OnlineLearning(
                            BackendType.VOWPAL_WABBIT,
                            *UNKNOWN,
                            inference_mode=ARGS.inference_mode,
                            classes=ast.literal_eval(ARGS.classes))
        elif self.backend == BackendType.PYTORCH.value:
            self.model = OnlineLearning(
                            backend_type=BackendType.PYTORCH,
                            pretrained_model=self.model_backend,
                            learning_rate=ARGS.learning_rate,
                            optimizer=ARGS.pytorch_optimizer,
                            loss_function=ARGS.loss_function,
                            *UNKNOWN
                            )
        elif self.backend == BackendType.TENSORFLOW.value:
            self.model = OnlineLearning(
                            backend_type=BackendType.TENSORFLOW,
                            pretrained_model=self.model_backend,
                            learning_rate=ARGS.learning_rate,
                            optimizer=ARGS.pytorch_optimizer,
                            loss_function=ARGS.loss_function,
                            object_detection_model=OBJECT_DETECTION_MODEL,
                            *UNKNOWN
                            )

    def prepare_data_shape(self, request: Dict, update_flag=False):
        """
        Function to pre
            Parameters:
                request: Dictionary with HTTP request with input features and outputs
                update_flag: Boolean flag to indicate if there is outputs in the request
        """
        data = {}
        if self.backend == BackendType.PYTORCH.value:
            data["inputs"] = torch.tensor(request["inputs"])
            if "float" in request["type"]:
                if update_flag is True:
                    data["outputs"] = torch.tensor(request["outputs"]).type(torch.float32)
                data["inputs"].type(torch.float32)
                data["inputs"] = data["inputs"].reshape(request["input_shape"])
            return data
        if self.backend == BackendType.TENSORFLOW.value:
            data["inputs"] = request["inputs"]
        return data

    def prepare_prediction(self, prediction):
        if self.backend == BackendType.PYTORCH.value:
            prediction = prediction.detach().numpy().tolist()
        elif self.backend == BackendType.TENSORFLOW.value:
            if OBJECT_DETECTION_MODEL:
                num_detections = int(prediction.pop('num_detections'))
                prediction = {key: value[0, :num_detections].numpy().tolist()
                            for key, value in prediction.items()}
            else:
                prediction = prediction.numpy().tolist()
        return prediction

    def predict(self, request: Dict) -> Dict:
        """
        HTTP endpoint function to perform a prediction
            Parameters:
                request: Dictionay wiht HTTP request with input features and outputs
        """
        if bool(request) is False:
            return {}

        logging.info("New request")
        if "outputs" in request.keys():
            # Perform an update
            logging.info("Model update")
            data = self.prepare_data_shape(request, update_flag=True)
            save_model = self.model.update_v2(
                                        data['inputs'],
                                        data["outputs"])
            if save_model is True:
                logging.info("Saving model")
                self.save_model()
            msg = {
                "Update": "Model updated"
            }
        else:
            # Perfom a prediction
            data = self.prepare_data_shape(request)
            if MONITORING and self.last_prediction != None:
                df = pd.DataFrame()
                df["prediction"] = [self.last_prediction]
                df["target"] = [data["inputs"].cpu().numpy().reshape([-1])[0]]
                requests.post(str(MONITORING_HOST)+"/monitor/regression_dataset",
                    json=df.to_dict(),
                    headers={'Content-Type': 'application/json'})

            self.last_prediction = self.prepare_prediction(self.model.predict_v2(data["inputs"]))
            logging.info(f"Prediction: {self.last_prediction}")
            if MONITORING:
                df = pd.DataFrame()
                power = []
                for sample in data["inputs"][0]:
                    power.append(float(sample))
                df["power"] = power
                print(df.head())
                print(df.info())
                requests.post(str(MONITORING_HOST)+"/monitor/data_drift_dataset",
                        json=df.to_dict(),
                        headers={'Content-Type': 'application/json'})

            msg = {
                "Prediction": self.last_prediction
            }
        return msg

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("--backend", type=str, default='pytorch',#'tensorflow',
                        help="""online learning backend framework. Options:
                        tensorflow, sklearn, vowpal_wabbit, pytorch""",
                        required=False)
    PARSER.add_argument("--inference_mode", type=str, default="regression",
                        help="regression or classification task", required=False)
    PARSER.add_argument("--classes", type=str, default="('dog', 'cat')",
                        help="list of classes for classification", required=False)
    # PARSER.add_argument("--minio_bucket", type=str, default="driver-profile",
    #                     help="minio bucket where save the model", required=False)
    #default="simple-kears-model"
    # PARSER.add_argument("--minio_model_name", type=str, default="smart_city_model.pt",#default="smart_energy_model_7_hours.pt",# default="smart_city_model.pt",
    #                     help="model name to save in minio", required=False)
    PARSER.add_argument("--minio_scaler_name", type=str, default="scaler.pkl",
                        help="scaler name to save in minio", required=False)
    PARSER.add_argument("--local_model_name", type=str, default="model",
                        help="model name to save locally", required=False)
    PARSER.add_argument("--local_scaler_name", type=str, default="scaler",
                        help="scaler name to save locally", required=False)
    # PARSER.add_argument("--ol_service_name", type=str,
    #                     default="ol-smart-grid-power-consumption",
    #                     help="ol service name", required=False)
    PARSER.add_argument("--connector_type", type=str, default="mqtt",
                        help="Techology for collecting data. Options: mqtt, kafka, rest, hybrid",
                        required=False)
    PARSER.add_argument("--topic_predictions", type=str, default="predictions",
                        required=False, help="topic where predictions are published")
    PARSER.add_argument("--learning_rate", type=float, default=1e-3,
                        required=False, help="learning rate to train")
    PARSER.add_argument("--pytorch_optimizer", type=str, default="adam",
                        required=False, help="optimizer to train pytorch nn. options: adam or sgd")
    PARSER.add_argument("--loss_function", type=str, default="mse",
                        required=False,
                        help="loss function to train pytorch nn. options: mse or mae")

    ARGS, UNKNOWN = PARSER.parse_known_args()

    logging.info("Starting online learning KServing")

    # model = KFServingModel(ARGS.ol_service_name)
    model = KFServingModel(KSERVE_MODEL_NAME)
    model.set_up()
    kserve.KFServer(workers=1).start([model])
