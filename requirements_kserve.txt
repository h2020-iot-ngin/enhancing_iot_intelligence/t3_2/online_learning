kafka-python==2.0.
APScheduler==3.9.1
paho-mqtt==1.5.1
h5py < 3.0.0
joblib==0.17.0
scikit-learn==0.24.2
vowpalwabbit==8.11.0
pandas==1.5.0
numpy==1.23.3
dill==0.3.5.1
kserve==0.7.0
tensorflow
minio
dill==0.3.5.1
python-dotenv
