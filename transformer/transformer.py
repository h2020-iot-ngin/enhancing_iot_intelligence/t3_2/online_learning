""" Transformer component powered by Kserve Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.
import logging
import os
import json
from typing import Dict
import requests
import kserve
from dotenv import load_dotenv
from apscheduler.schedulers.background import BackgroundScheduler
from .preprocessing import PreprocessingTools
from ..utils.data.minio_client import MinioConnector
from ..utils.ml.load_save_model import ModelLoader
from ..utils.data.streams_connector import Streamer

load_dotenv()

KSERVE_SERVER_HOST=os.getenv('KSERVE_SERVER_HOST')
KSERVE_MODEL_NAME=os.getenv('KSERVE_MODEL_NAME')
KSERVE_TRANSFORMER_PORT=os.getenv('KSERVE_TRANSFORMER_PORT')
MINIO_USER = os.getenv("MINIO_ACCESS_KEY")
MINIO_PASS = os.getenv('MINIO_SECRET_KEY')
MINIO_HOST = os.getenv('MINIO_HOST')
MINIO_BUCKET = os.getenv('MINIO_BUCKET')
MINIO_SCALER_NAME = os.getenv('MINIO_SCALER_NAME')
STREAMING_CONNECTOR_TYPE = os.getenv('STREAMING_CONNECTOR_TYPE')
BROKER_URL = os.getenv('BROKER_URL')
BROKER_PORT = os.getenv('BROKER_PORT')
BROKER_USER = os.getenv('BROKER_USER')
BROKER_KEY = os.getenv('BROKER_KEY')
PREDICTIONS_TOPIC = os.getenv('PREDICTIONS_TOPIC')

LOOKBACK = os.getenv("LOOKBACK")

MINIO_CONNECTOR = MinioConnector(
                            MINIO_USER,
                            MINIO_PASS,
                            MINIO_HOST)


class Transformer(kserve.KFModel):
    """Transformer KFserving/Kserve class"""
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.predictor_host = predictor_host
        self.model_name = name
        self.scaler = None
        self.minio_client = None
        self.scaler_loader = None
        self.model_bucket = None
        self.local_scaler_name = './scaler'
        self.inference_request_flag = False
        self.publisher_client = None
        self.last_prediction=None
        self.predictions_topic=None
        self.set_up()

    def set_up(self):
        """ Set up function """
        self.load()
        self.create_publisher()
        self.preprocessing_tools = PreprocessingTools(lookback=int(LOOKBACK),scaler=self.scaler)
        scheduler = BackgroundScheduler()
        scheduler.add_job(func=self.inference_or_update_request, trigger="interval", minutes=60)
        scheduler.start()

    def inference_or_update_request(self):
        """ Inference request once the data is ready """
        preprocessing_outputs = self.preprocessing_tools.create_data_to_update()
        # Update model step
        if preprocessing_outputs['update'] is True:
            logging.info("Data prepared to perform an update")
            update_request = {
                "inputs":preprocessing_outputs['update_inputs'],
                "outputs":preprocessing_outputs['update_outputs'],
                "input_shape": preprocessing_outputs['input_shape'],
                "type": preprocessing_outputs["type"]
            }
            r = requests.post(
                    'http://'+self.predictor_host+'/v1/models/'+self.model_name+':predict',
                    json=update_request,
                    headers={'Content-Type': 'application/json'},
                    timeout=10)
        # Inference step
        if preprocessing_outputs['predict'] is True:
            logging.info("Data prepared to perform an inference")
            pred_request = {
                        "inputs": preprocessing_outputs['prediction_inputs'],
                        "input_shape": preprocessing_outputs['input_shape'],
                        "type": preprocessing_outputs["type"]
                            }
            r = requests.post(
                    'http://'+self.predictor_host+'/v1/models/'+self.model_name+':predict',
                    json=pred_request,
                    headers={'Content-Type': 'application/json'},
                    timeout=10)
            ol_output = {}
            ol_output['power_cons_pred'] = self.postprocess(r.json())
            self.publisher_client.publish_one(
                                        self.predictions_topic,
                                        value=str(ol_output))

    def load(self):
        """ Loads scaler from MinIO """
        self.scaler_loader = ModelLoader()
        minio_client = MINIO_CONNECTOR.get_client()
        minio_client.fget_object(MINIO_BUCKET, MINIO_SCALER_NAME, self.local_scaler_name)
        self.scaler = self.scaler_loader.load_scaler(self.local_scaler_name)

    def preprocess(self, inputs: Dict) -> Dict:
        logging.info("New data available")
        logging.info("Performing preprocessing step")
        logging.info(f"{inputs}")
        logging.info(f"{type(inputs)}")
        if isinstance(inputs,dict) == False:
            # if "data" in input.keys():
            inputs = inputs.data
            inputs = json.loads(inputs.decode("utf-8").replace("'",'"'))
        # if bool(inputs):
            self.preprocessing_tools.compute_power(inputs)
        return {}

    def postprocess(self, inputs: Dict) -> Dict:
        logging.info("Postprocess step")
        if "Prediction" in inputs:
            self.last_prediction = self.preprocessing_tools.inverse_scaling(
                                                                float(
                                                                    inputs["Prediction"]
                                                                    )
                                                                )[0][0]
        if self.last_prediction is None:
            return {"Prediction": "None"}
        return {"Prediction":str(self.last_prediction)}

    def create_publisher(self):
        """ Creates streaming publisher in a specific topic
        depending on the conntector type (MQTT or Kafka) """
        logging.info("Creating Streaming connector client")
        print(STREAMING_CONNECTOR_TYPE)
        if STREAMING_CONNECTOR_TYPE == "mqtt":
            connector = Streamer(Streamer.ConnectorTypes.MQTT).connector
            self.publisher_client = connector.Publisher(
                        broker_address=BROKER_URL,
                        port=int(BROKER_PORT),
                        username=BROKER_USER,
                        password=BROKER_KEY
                        )
        if STREAMING_CONNECTOR_TYPE == 'kafka':
            connector = Streamer(Streamer.ConnectorTypes.KAFKA).connector
            self.publisher_client = connector.Publisher(
                        broker_address=BROKER_URL,
                        port=int(BROKER_PORT))
        self.predictions_topic = PREDICTIONS_TOPIC

if __name__ == "__main__":
    logging.info("Predictor Host: %s", KSERVE_SERVER_HOST)
    logging.info("Model Name: %s", KSERVE_MODEL_NAME)

    transformer = Transformer(
                            KSERVE_MODEL_NAME,
                            predictor_host=KSERVE_SERVER_HOST
                            )
    kfserver = kserve.KFServer(http_port=KSERVE_TRANSFORMER_PORT)
    kfserver.start(models=[transformer])
