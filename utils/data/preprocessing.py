""" Preprocessing data Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import logging
import torch
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

logging.basicConfig(level=logging.INFO)

class PreprocessingTools():
    """ Preprocessing tools class"""
    def __init__(self, lookback=None, horizon=None, scaler=None):
        """
        Constructor
            Parameters
                Inputs:
                    - lookback
                    - horizon
                    - scaler
        """
        self.n_data = 0
        self.average_power = 0
        self.power_time_series = []
        self.data_2_predict = False
        self.data_2_update = False

        if lookback is None:
            self.lookback = 36
        else:
            self.lookback = lookback

        if horizon is None:
            self.horizon = 24
        else:
            self.horizon = horizon

        if scaler is None:
            self.scaler = StandardScaler()
            self.scaler_partial_fit = True
        else:
            self.scaler = scaler
            self.scaler_partial_fit = False

    def get_scaler(self):
        """
        Scaler getting function
            Parameters
                Outputs:
                    scaler
        """
        return self.scaler

    def compute_power(self, data:dict):
        """
        Computing power function
            Parameters
                Inputs
                    - data
        """
        logging.info("Data received")
        data = pd.DataFrame.from_dict(data, orient='index')
        data = data.transpose()
        try:
            power_a = data['vm1']*data['im1']*np.cos(np.radians(float(data['vph1']-data['iph1'])))
            power_b = data['vm2']*data['im2']*np.cos(np.radians(float(data['vph2']-data['iph2'])))
            power_c = data['vm3']*data['im3']*np.cos(np.radians(float(data['vph3']-data['iph3'])))
            power = power_a + power_b + power_c
            self.n_data += 1
            if self.scaler_partial_fit is True:
                self.scaler.partial_fit(np.array(power[0]).reshape(1,-1))
            self.update_average_power(power[0])
        except Exception as e:
            logging.exception("Error: %s",str(e))

    def update_average_power(self, power):
        """
        Update average power
            Parameters
                Inputs:
                    - power
        """
        self.average_power = (self.average_power * self.n_data + power) / (self.n_data+1)

    def data_scaling(self):
        """
        Data scalation function
            Parameters
                Outputs:
                    - scaled data
        """
        return self.scaler.transform(np.array(self.power_time_series).reshape(-1,1))

    def inverse_scaling(self, inputs):
        """
        Inverse data scalation function
            Parameters
                Inputs:
                    - inputs
                Outputs:
                    - inverse scaled data
        """
        return self.scaler.inverse_transform(np.array(inputs).reshape(-1,1))

    def prepare_time_series(self):
        """
        Function to create power time series
        """
        if self.power_time_series is not None:
            if len(self.power_time_series) > self.lookback + self.horizon:
                self.power_time_series.pop(0)
                self.data_2_update = True
            self.power_time_series.append(self.average_power)
            if len(self.power_time_series) >= self.lookback:
                # Perform a prediction
                self.data_2_predict = True
                # self.data_ready = True

    def preprocess_new_data(self, data):
        """
        Preprocess new data function
        """
        self.compute_power(data)

    def create_data_to_update(self):
        """
        Function to create batch for training.
        This function is runned each hour:
            1- Prepare time series
            2- Reset avg power
            3- Scale time series
            4- If enough data to perform prediction return inputs
            5- If enough data to update return inputs and outputs

            Parameters
                Outputs:
                    - input data to train the model
        """

        logging.info("Creating time series")
        self.prepare_time_series()
        self.average_power = 0
        self.n_data = 0

        output = {
                "update":self.data_2_update,
                "predict":self.data_2_predict
                }

        if self.data_2_predict is True:
            scaled_data = self.data_scaling()
            prediction_inputs = torch.tensor(scaled_data[-self.lookback:])
            prediction_inputs = prediction_inputs.type(torch.float32)
            prediction_inputs = prediction_inputs.reshape(1,36,1)
            output["prediction_inputs"] = prediction_inputs

        if self.data_2_update is True:
            update_inputs = torch.tensor(scaled_data[:self.lookback])
            update_outputs = torch.tensor(scaled_data[-1])
            update_inputs = update_inputs.type(torch.float32)
            update_outputs = update_outputs.type(torch.float32)
            update_inputs = update_inputs.reshape(1,36,1)
            output["update_inputs"] = update_inputs
            output["update_outputs"] = update_outputs

        return output
