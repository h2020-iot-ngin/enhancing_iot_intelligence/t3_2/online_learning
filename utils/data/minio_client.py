""" Minio Connector Module. """
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import os
import logging
import certifi
import urllib3
from minio import Minio

logging.basicConfig(level=logging.INFO)

class MinioConnector():
    """ Minio Connector Class"""
    def __init__(self, user, password, url):
        """
        Constructor
            Parameters
                Inputs:
                    - user
                    - password
                    - url
        """
        self.user = user
        self.url = url
        self.key = password
        self.minio_client = self.get_minio_client(
                                        self.user,
                                        self.key,
                                        self.url)

    def get_minio_client(
                        self,
                        user, password,
                        url, minio_secure=True,
                        minio_region='es',
                        ignore_cert_warning=False):
        """
        Get MinIO client
            Parameters
                Inputs:
                    - user
                    - password
                    - url
                    - minio_secure
                    - minio_region
                    - ignore_cert_warning
                Outputs:
                    - MinIO client
        """
        ca_certs = os.environ.get('SSL_CERT_FILE') or certifi.where()
        cert = 'CERT_REQUIRED'
        _http = urllib3.PoolManager(
            timeout=1,
            maxsize=10, #minio.helpers.MAX_POOL_SIZE,
            cert_reqs=cert,
            ca_certs=ca_certs,
            retries=urllib3.Retry(
                total=3,
                backoff_factor=0.2,
                status_forcelist=[500, 502, 503, 504]
            )
        )
        minio_client = Minio(url,
                        access_key=user,
                        secret_key=password,
                        secure=minio_secure,
                        http_client=_http,
                        region=minio_region)

        return minio_client

    def get_client(self):
        """
        Get MinIO client function
            Parameters
                Outputs:
                    - minio_client
        """
        return self.minio_client

    def download_model(self, bucket_name, minio_model_name, local_model_name):
        """
        Download model from MinIO
            Parameters
                Outputs:
                    - bucket name: MinIO bucket where the model is saved
                    - minio_model_name: Model name at MinIO
                    - local_model_name: Model name to save in local
        """
        try:
            self.minio_client.fget_object(bucket_name, minio_model_name, local_model_name)
        except ValueError as e:
            logging.exception(
                        "Multiple files must be downloaded to save the model locally: %s",
                        str(e))
            self.download_files(bucket_name, minio_model_name)

    def download_files(self, bucket_name, model_name):
        """
        Download function to use when multiple files are needed to load
        the model form MinIO.
            Parameters
                Outputs:
                    - bucket name: MinIO bucket where the model is saved
                    - minio_model_name: Model name at MinIO
                    - local_model_name: Model name to save in local
        """
        objects = self.minio_client.list_objects(bucket_name, recursive=True)
        for obj in objects:
            if model_name in obj.object_name:
                self.minio_client.fget_object(bucket_name, obj.object_name, obj.object_name)
