""" Streams Connector Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import random
import string
from enum import Enum
from json import loads, dumps
from kafka import KafkaConsumer, KafkaProducer
import paho.mqtt.client as mqtt

def get_random_string(length):
    """
    Random string generation
        Parameters
            Inputs:
                - length
            Outputs
                - random string
    """
    # With combination of lower and upper case
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

class KafkaConnector():
    """ Kafka Connector Class """
    class Consumer():
        """ Kafka Consumer Class """
        consumer = None
        def __init__(self, topic_names:list, broker_address:str='localhost',
                    port:int=9092, client_id:str=None, username:str=None,
                    password:str=None):
            """
            Constructor
                Parameters
                    Inputs:
                        - topic_names
                        - broker_address
                        - port
                        - client_id
            """
            self._client_id = get_random_string(16) if client_id is None else client_id
            server_address = broker_address +':' + str(port)
            print(f'Connecting to kafka broker at {server_address}')
            if username is not None and password is not None:
                self.consumer = KafkaConsumer(bootstrap_servers=server_address,
                        client_id=self._client_id,
                        auto_offset_reset='earliest',
                        enable_auto_commit=True,
                        auto_commit_interval_ms=1000,
                        group_id='my-group',
                        # request_timeout_ms=10000,
                        value_deserializer=lambda x: loads(x.decode('utf-8')),
                        sasl_plain_username=username,
                        sasl_plain_password=password)
            else:
                self.consumer = KafkaConsumer(bootstrap_servers=server_address,
                        client_id=self._client_id,
                        auto_offset_reset='earliest',
                        enable_auto_commit=True,
                        auto_commit_interval_ms=1000,
                        group_id='my-group',
                        # request_timeout_ms=10000,
                        value_deserializer=lambda x: loads(x.decode('utf-8')))
            self.subscribe_topic(topic_names=topic_names)

        def consume_one(self):
            """
            Consume 1 message
                Parameters
                    Outputs:
                        - received message
            """
            return self.consumer.next_v1()

        def generator(self):
            """
            Generator
                Parameters
                    Outputs:
                        - kafka consumer
            """
            return self.consumer

        def remove_consumer(self):
            """
            Consume remove
            """
            self.consumer.close()
            self.consumer = None

        def subscribe_topic(self, topic_names:list):
            """
            Topic subscription function
            """
            self.consumer.subscribe(topics=topic_names)

    class Publisher():
        """ Kafka Publisher Class"""
        def __init__(self, broker_address:str='localhost', port:int=9092, client_id:str=None,
                    username:str=None, password:str=None):
            """
            Constructor
                Parameters
                    Inputs:
                        - broker_address
                        - port
                        - client_id
            """
            self._client_id = get_random_string(16) if client_id is None else client_id
            server_address = broker_address +':' + str(port)
            if username is not None and password is not None:
                self._producer = KafkaProducer(bootstrap_servers=[server_address],
                                        client_id=self._client_id,
                                        value_serializer = lambda x: dumps(x).encode('utf-8'),
                                        sasl_plain_username=username,
                                        sasl_plain_password=password)
            else:
                self._producer = KafkaProducer(bootstrap_servers=[server_address],
                        client_id=self._client_id,
                        value_serializer = lambda x: dumps(x).encode('utf-8'))

        def publish_one(self, topic_name:str, value):
            """
            Publish message
                Parameters
                    Inputs:
                        - topic_name
                        - value
                    Outputs:
                        - Message delivery ack
            """
            return self._producer.send(topic_name, value)

        def remove_publisher(self):
            """
            Remove publisher
            """
            self._producer.close()
            self._producer = None

class MQTTConnector():
    """ MQTT Connector Class """
    class Consumer():
        """ MQTT Consumer Class """
        consumer = None
        messages = None

        def __init__(self, topic_names:list, broker_address:str='localhost', port:int=1883,
                    username:list=None, password:list=None):
            """
            Constructor
                Parameters
                    Inputs:
                        - topic_names
                        - broker_address
                        - port
                        - username
                        - password
            """
            self.messages = []

            self.consumer = mqtt.Client() #create new instance
            self.consumer.on_message=self.on_message
            self.consumer.on_connect=self._on_connect
            if username is not None and password is not None:
                self.consumer.username_pw_set(username, password) # When user & password is required
            self.consumer.connect(broker_address, port, keepalive=60) #connect to broker
            # self.consumer.subscribe(("to_update", 0))
            for topic in topic_names:
                self.consumer.subscribe(topic)

        def on_message(self, client, userdata, message):
            """
            Callback function when message is received.
            The message is stored in message queue
                Parameters
                    Inputs:
                        - client
                        - userdata
                        - message
            """
            self.messages.append(message.payload.decode("utf-8"))

        def _on_connect(self, client, userdata, flags, rc):
            """
            Callback function when connecting client.
                Parameters
                    Inputs:
                        - client
                        - userdata
                        - flags
                        - rc
            """
            if rc != 0:
                print("Error connecting to MQTT client")
            else:
                print("Consumer connected successfully")

        def consume_one(self):
            """
            Function to consume one message from the queue.
                Parameters
                    Outputs:
                        - message
            """
            return self.messages.pop() if len(self.messages) >0 else None

        def generator(self):
            """
            Generator function
                Parameters
                    Outputs:
                        - return all messages
            """
            return iter(self.messages)

        def remove_consumer(self):
            """
            Remove consumer function
            """
            self.consumer.loop_stop()
            self.consumer = None
            self.messages.clear()

    class Publisher():
        """ MQTT Pusblisher Class """
        def __init__(self, broker_address:str='localhost', port:int=1883,
                    username:list=None, password:list=None):
            """
            Constructor
                Parameters
                    Inputs:
                        - broker_address
                        - port
                        - username
                        - password
            """
            #create new instance
            self._producer = mqtt.Client()
            if username is not None and password is not None:
                # When user & password is required
                self._producer.username_pw_set(username, password)
            #connect to broker
            self._producer.connect(broker_address, port, keepalive=60)
            self._producer.on_connect = self._on_connect
            self._producer.loop_start()

        def publish_one(self, topic_name:str, value):
            """
            Function to publish a message
                Parameters
                    Inputs:
                        - topic_name
                        - value
                    Outputs:
                        - Message delivery ack
            """
            return self._producer.publish(topic_name, payload=value)

        def remove_publisher(self):
            """
            Remove publisher
            """
            self._producer.loop_stop()
            self._producer = None

        def _on_connect(self, client, userdata, flags, rc):
            """
            Callback function when connecting client.
                Parameters
                    Inputs:
                        - client
                        - userdata
                        - flags
                        - rc
            """
            if rc != 0:
                print("Error connecting to MQTT client")
            else:
                print("Publisher connected successfully")

class Streamer():
    """ Streamer Class """
    class ConnectorTypes(Enum):
        """ Connector Types Class """
        KAFKA = 'kafka'
        MQTT = 'mqtt'

    def __init__(self, connector_type:ConnectorTypes = None):
        """
        Constructor
            Parameters
                Inputs:
                    - connector_type
        """
        if connector_type:
            if connector_type.value == Streamer.ConnectorTypes.KAFKA.value:
                self.connector = KafkaConnector()
            elif connector_type.value == Streamer.ConnectorTypes.MQTT.value:
                self.connector = MQTTConnector()
        else:
            self.connector = None

    def get_connector(self, connector_type:ConnectorTypes):
        """
        Get connector function
            Parameters
                Inputs:
                    - connector_type
                Outputs:
                    - connector
        """
        if connector_type.value == Streamer.ConnectorTypes.KAFKA:
            connector = KafkaConnector()
        elif connector_type.value == Streamer.ConnectorTypes.MQTT:
            connector = MQTTConnector()
        return connector
