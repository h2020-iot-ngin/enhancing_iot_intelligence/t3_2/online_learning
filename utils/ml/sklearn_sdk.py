""" Sklearn backend Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import joblib
import numpy as np


class SkLearnModel():
    """Sklearn Model Backend Class"""
    model = None
    algorithm = ''
    inference_mode = ''

    def __init__(self, model: object = None, inference_mode: str = 'regression',
                algorithm: str = 'sgd', classes=None, **kwargs):
        """
        Constructor
            Parameters
                Inputs:
                    - model
                    - inference_mode: Regression or classification
                    - algorithm
                    - classes: Only needed for classification
        """
        if model is not None:
            if isinstance(model, str):
                self.model = joblib.load(model)
            else:
                self.model = model
        else:
            if inference_mode == 'regression':
                if algorithm.lower().replace(' ', '') == 'passiveagressive' or algorithm.lower().replace(' ', '') == 'pa':
                    from sklearn.linear_model import PassiveAggressiveRegressor
                    self.model = PassiveAggressiveRegressor(**kwargs)
                elif algorithm.lower().replace(' ', '') == 'multilayerperceptron' or algorithm.lower().replace(' ', '') == 'mlp':
                    from sklearn.neural_network import MLPRegressor
                    self.model = MLPRegressor(**kwargs)
                elif algorithm.lower().replace(' ', '') == 'stochasticgradientdescent' or algorithm.lower().replace(' ', '') == 'sgd':
                    from sklearn.linear_model import SGDRegressor
                    self.model = SGDRegressor(**kwargs)
                else:
                    raise Exception(
                        "Error: algorithm not understood, valid values are 'pa' (passive agressive), 'mlp' (multilayer perceptron) or 'sgd' (stochastic gradient descent).")
            elif inference_mode == 'classification':
                if algorithm.lower().replace(' ', '') == 'passiveagressive' or algorithm.lower().replace(' ', '') == 'pa':
                    from sklearn.linear_model import PassiveAggressiveClassifier
                    self.model = PassiveAggressiveClassifier(**kwargs)
                elif algorithm.lower().replace(' ', '') == 'multilayerperceptron' or algorithm.lower().replace(' ', '') == 'mlp':
                    from sklearn.neural_network import MLPClassifier
                    self.model = MLPClassifier(**kwargs)
                elif algorithm.lower().replace(' ', '') == 'stochasticgradientdescent' or algorithm.lower().replace(' ', '') == 'sgd':
                    from sklearn.linear_model import SGDClassifier
                    self.model = SGDClassifier(**kwargs)
                else:
                    raise Exception(
                        "Error: algorithm not understood, valid values are 'pa' (passive agressive), 'mlp' (multilayer perceptron) or 'sgd' (stochastic gradient descent).")
            else:
                raise Exception(
                    "Error: inference mode not understood, valid values are 'regression' or 'classification'")

            self.algorithm = algorithm
        self.inference_mode = inference_mode
        self.classes = classes

    def __call__(self, inputs: list, outputs: list = None) -> list:
        """
        Call function
            Parameters
                Inputs:
                    - inputs
                    - outputs
                Outputs:
                    - model outputs
        """
        if outputs is not None:
            if self.inference_mode == 'classification':
                return self.model.partial_fit(inputs, outputs, classes=np.arange(len(self.classes)))
            return self.model.partial_fit(inputs, outputs)
        return self.model.predict(inputs).tolist()

    def set_classes(self, classes: list):
        """
        Classes setting function
            Parameters
                Inputs:
                    - classes
        """
        self.classes = classes

    def set_model(self, model: object):
        """
        Model setting function
            Parameters
                Inputs:
                    - model
        """
        if isinstance(model,str):
            self.model = joblib.load(model)
        else:
            self.model = model

    def get_model(self):
        """
        Get model function
            Parameters
                Outputs:
                    - model
        """
        return self.model

    def get_inference(self, inputs: list):
        """
        Get inference function
            Parameters
                Inputs:
                    - inputs
                Outputs:
                    - inferences
        """
        return self.model.predict(inputs).tolist()

    def update_model(self, inputs: list, outputs: list):
        """
        Update model function
            Parameters
                Inputs:
                    - inputs
                    - outputs
        """
        try:
            self.model.partial_fit(
                inputs, outputs, classes=np.arange(len(self.classes)))
        except Exception as e:
            print("Error calling the partial_fit functionality: " + str(e))
