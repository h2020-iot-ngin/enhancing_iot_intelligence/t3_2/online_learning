""" Vowpal Wabbit backend Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

from pandas.core.frame import DataFrame
from vowpalwabbit import pyvw
from vowpalwabbit.sklearn_vw import tovw
# from vowpalwabbit.DFtoVW import DFtoVW, SimpleLabel, MulticlassLabel, Feature

import joblib

class VowpalWabbitModel():
    """ Vowpal Wabbit Model Backend """
    model = None
    algorithm = ''
    inference_mode = ''
    data_shape = None

    def __init__(self, model:object = None, inference_mode:str = 'regression',
                num_classes=None, **kwargs):
        """
        Constructor
            Parameters
                Intputs:
                    - model
                    - inference_mode
                    - num_classes
        """
        if model is not None:
            if isinstance(model, str):
                self.model = joblib.load(model)
            else:
                self.model = model
        else:
            if inference_mode == 'classification':
                # Setting up Vowpal Wabbit with One Against all (oaa)
                # classification and Hinge loss function
                self.model = pyvw.vw(arg_str="--oaa " + str(num_classes) + " --loss_function=hinge",
                                    quiet=True, power_t=0.2, **kwargs)
            else:
                self.model = pyvw.vw(quiet=True, power_t=0.2, **kwargs)
            self.inference_mode = inference_mode

    def __call__(self, inputs:DataFrame, outputs:DataFrame = None) -> list:
        """
        Callback function
            Parameters
                Inputs:
                    - inputs
                    - outputs
                Outputs:
                    None or predictions
        """
        data = tovw(x=inputs, y=outputs)

        if len(inputs) == 1: data = data[0]

        if outputs is not None:
            return self.model.learn(data)

        return self.model.predict(data)

    def set_model(self, model:object):
        """
        Set model function
            Parameters
                Inputs:
                    - model
        """
        if isinstance(model,str):
            self.model = joblib.load(model)
        else:
            self.model = model

    def get_model(self):
        """
        Get model function
            Parameters
                Outputs:
                    - model
        """
        return self.model

    def get_inference(self, inputs:list):
        """
        Get inference function
            Parameters
                Inputs:
                    - inputs
                Outputs:
                    - predictions
        """
        data_vw = tovw(x=inputs)
        return self.model.predict(data_vw)

    def update_model(self, inputs:list, outputs:list):
        """
        Update model function
            Parameters
                Inputs:
                    - inputs
                    - outputs
        """
        data_vw = tovw(x=inputs, y=outputs)

        self.model.learn(data_vw)
