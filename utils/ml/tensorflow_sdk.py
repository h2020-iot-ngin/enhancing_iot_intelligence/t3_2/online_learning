""" Tensorflow backend Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import tensorflow as tf

class TensorflowModel():
    """
    Tensorflow Model Backend Class
    """
    model = None
    algorithm = ''
    inference_mode = ''
    data_shape = None
    optimizer = None
    loss = None
    metrics = None

    def __init__(self, model:object = None, optimizer ="adam", loss_function = "mse",
                metrics = None, learning_rate=1e-3, object_detection_model=None):
        """
        Constructor
            Parameters:
                Inputs:
                    - model
                    - optimizer
                    - loss: loss function
                    - metrics: metrics to monitor performance
                    - learning_rate: learning rate to train NN.
        """
        if model is not None:
            self.model = model
        self.optimizer_definition(optimizer, learning_rate)
        self.loss_fn_definition(loss_function)
        self.object_detection_model = object_detection_model
        self.metrics_definition(metrics)
        print(self.object_detection_model)
        if not self.object_detection_model:
            self.compile_model()

    def optimizer_definition(self, optimizer, learning_rate):
        """
        Optimizer definition function
            Parameters
                Inputs:
                    - optimizer
        """
        if optimizer == "adam":
            self.optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
        elif optimizer == "sgd":
            optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate)
        else:
            raise Exception("Optimizer not supported. Try adam or sgd")

    def loss_fn_definition(self, loss):
        """
        Loss definition function
            Parameters
                Inputs:
                    - loss: Loss function
        """
        if loss == "mse":
            self.loss = tf.keras.losses.MeanSquaredError()
        elif loss == "cross_entropy":
            self.loss = tf.keras.losses.CategoricalCrossentropy()
        else:
            raise Exception("Loss function not supported. Try adam or sgd")

    def metrics_definition(self, metrics):
        """
        Metrics definition function
            Parameters
                Inputs:
                    - metrics
        """
        self.metrics = []
        if metrics is None:
            self.metrics.append("mse")
        else:
            if 'mse' in metrics:
                self.metrics.append("mse")
            if 'accuracy' in metrics:
                self.metrics.append('accuracy')

    def get_model(self):
        """
        Function to return the model
        """
        return self.model

    def set_optimizer(self, optimizer:object):
        """
        Function to set the optimizer
            Parameters
                Inputs:
                    - optimizer
        """
        self.optimizer = optimizer

    def set_loss(self, loss:object):
        """
        Function to set the loss
            Parameters
                Inputs:
                    - loss
        """
        self.loss = loss

    def set_metrics(self, metrics:list):
        """
        Function to set the metrics
            Parameters
                Inputs:
                    - metrics
        """
        self.metrics = metrics

    def get_inference(self, inputs):
        """
        Function to perform an inference
            Parameters
                Inputs:
                    - inputs: input data
                Outputs:
                    - inferences
        """
        if self.object_detection_model:
            return self.model(inputs)
        return self.model.predict(inputs)
    
    def predict(self, inputs):
        """
        Function to perform an inference
            Parameters
                Inputs:
                    - inputs: input data
                Outputs:
                    - inferences
        """
        if self.object_detection_model:
            return self.model(inputs)
        return self.model.predict(inputs)

    def update_model(self, inputs, outputs):
        """
        Update model function
            Parameters
                Inputs:
                    - inputs
                    - outputs
        """
        losses, metric = self.model.train_on_batch(inputs, outputs)
        return losses # returns losses, accuracy

    def compile_model(self):
        """
        Compile NN function
        """
        self.model.compile(optimizer=self.optimizer, loss=self.loss, metrics=self.metrics)
