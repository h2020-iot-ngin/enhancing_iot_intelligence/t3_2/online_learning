""" Pytorch backend Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import torch

class PytorchModel():
    """Pytorch Model Backend Class"""
    model = None
    loss = None
    optimizer = None
    metrics = None

    def __init__(self, model:object = None, optimizer = "adam",
                loss_function = "mse", metrics = None, learning_rate=1e-3):
        """
        Constructor
            Parameters:
                Inputs:
                    - model
                    - optimizer
                    - loss: loss function
                    - metrics: metrics to monitor performance
                    - learning_rate: learning rate to train NN.
        """

        if model is not None:
            self.model = model
        self.optimizer_definition(optimizer, learning_rate)
        self.loss_fn_definition(loss_function)
        self.metrics_definition(metrics)

    def optimizer_definition(self, optimizer, learning_rate):
        """
        Optimizer definition function
            Parameters
                Inputs:
                    - optimizer
        """
        if optimizer == "adam":
            self.optimizer = torch.optim.Adam(
                                    self.model.parameters(),
                                    lr=learning_rate)
        elif optimizer == "sgd":
            self.optimizer = torch.optim.SGD(
                                    self.model.parameters(),
                                    lr=learning_rate, momentum=0.9)
        else:
            raise Exception("Optimizer not supported. Try adam or sgd")

    def loss_fn_definition(self, loss):
        """
        Loss definition function
            Parameters
                Inputs:
                    - loss: Loss function
        """
        if loss == "mse":
            self.loss = torch.nn.MSELoss()
        elif loss == "cross_entropy":
            self.loss = torch.nn.CrossEntropyLoss()
        else:
            raise Exception("Loss function not supported. Try adam or sgd")

    def metrics_definition(self, metrics):
        """
        Metrics definition function
            Parameters
                Inputs:
                    - metrics
        """
        self.metrics = []
        if metrics is None:
            self.metrics.append("mse")
        else:
            if 'mse' in metrics:
                self.metrics.append("mse")
            if 'accuracy' in metrics:
                self.metrics.append('accuracy')

    def get_model(self):
        """
        Function to return the model
        """
        return self.model

    def set_optimizer(self, optimizer:object):
        """
        Function to set the optimizer
            Parameters
                Inputs:
                    - optimizer
        """
        self.optimizer = optimizer

    def set_loss(self, loss:object):
        """
        Function to set the loss
            Parameters
                Inputs:
                    - loss
        """
        self.loss = loss

    def set_metrics(self, metrics:list):
        """
        Function to set the metrics
            Parameters
                Inputs:
                    - metrics
        """
        self.metrics = metrics

    def get_inference(self, inputs):
        """
        Function to perform an inference
            Parameters
                Inputs:
                    - inputs: input data
                Outputs:
                    - inferences
        """
        self.model.eval()
        inferences = self.model(inputs)
        return inferences

    def update_model(self, inputs, outputs):
        """
        Function to update the model
            Parameters
                Inputs:
                    inputs: Input data
                    outputs: Actual target data
                Outputs:
                    Training losses
        """
        if outputs is not None:
            self.model.train()
            model_outputs = self.model(inputs)
            self.optimizer.zero_grad()
            train_loss = self.compute_loss(outputs, model_outputs)
            train_loss.backward()
            self.optimizer.step()
            return train_loss.item() # float variable

    def predict(self, inputs):
        """
        Function to perform an inference
            Parameters
                Inputs:
                    - inputs: input data
                Outputs:
                    - inferences
        """
        self.model.eval()
        return self.model(inputs)

    def compute_loss(self, actual_data, model_outputs):
        """
        Function to compute training losses
            Parameters
                Inputs:
                    - actual_data: Actual target data
                    - model_outputs: Inferences performed by the model
                Outputs:
                    - losses
        """
        if model_outputs.dim() == 1:
            model_outputs = model_outputs.reshape(-1,1)
        if actual_data.dim() == 1:
            actual_data = actual_data.reshape(-1,1)
        if actual_data.shape[1] == 1:
            return self.loss(model_outputs, torch.Tensor(actual_data.reshape([-1,1])))
        return self.loss(model_outputs, torch.Tensor(actual_data.squeeze()))
