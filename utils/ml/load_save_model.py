""" Load/Save model Module"""
#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

import pickle
import joblib
import torch
import dill
import importlib
import tensorflow as tf

class ModelLoader():
    """Model Loader Class: It must load model"""
    def __init__(self, local_model_name=None, backend=None,
                module_name=None, recursive=False):
        """Constructor"""
        self.backend = backend
        self.local_model_name = local_model_name
        self.module_name = module_name
        self.recursive = recursive

    def load_model(self):
        """
        Load model depending on the backend
            Parameters
            ----------
            inputs :
                    None
            outputs :
                    model
        """
        if self.backend == "pytorch":
            if self.module_name is not None:
                backend_module = importlib.import_module(self.module_name)
                model = backend_module.model()
                model.load_state_dict(torch.load(self.local_model_name))
                return model
            return torch.jit.load(self.local_model_name)
        if self.backend == "tensorflow":
            if self.module_name is not None:
                backend_module = importlib.import_module(self.module_name)
                model = backend_module.model()
                model.load_weights(self.local_model_name)
            else:
                if not self.recursive:
                    model = tf.keras.models.load_model(self.local_model_name)
                else:
                    model = tf.saved_model.load(self.local_model_name+"/saved_model")
            return model
        if self.backend == "sklearn":
            return joblib.load(self.local_model_name)
        if self.backend == "alibi":
            return dill.loads(self.local_model_name)
        return None

    def load_scaler(self,scaler_path):
        """
        Load max-min scaler

            Parameters
            ----------
            inputs :
                    scaler_path
            outputs:
                    scaler
        """
        with open(scaler_path, 'rb') as scaler_f:
            return pickle.load(scaler_f)

class ModelSaver():
    """Model Saver Class"""
    def __init__(self, local_model_name=None,backend=None, module_name=None):
        """Constructor"""
        self.backend = backend
        self.local_model_name = local_model_name
        self.module_name = module_name

    def save_model(self, model):
        """
        Save model depending on the backend
            Parameters
            ----------
            inputs :
                    model
            outputs :
                    None
        """
        if self.backend == "pytorch":
            if self.module_name is not None:
                torch.save(model.state_dict(), self.local_model_name)
            else:
                torch.jit.save(model, self.local_model_name)
        if self.backend == "sklearn":
            joblib.dump(model, self.local_model_name)

    def save_scaler(self,scaler):
        """
        Save max-min scaler

            Parameters
            ----------
            inputs :
                    scaler
            outputs :
                    None
        """
        with open('scaler', 'wb') as scaler_f:
            pickle.dump(scaler, scaler_f)
