# Online Learning SDK for IoT-NGIN

Througout this readme, it will be explained the usage of the `OnlineLearning` SDK and a set of examples of usage programatically will be shown.

## Using the OnlineLearning SDK

The [OnlineLearning SDK](./OnlineLearning.py) is a python class acting as a wrapper, or a high level interface, of a set of online learning (OL) algorithms accessible through these AI frameworks:

- [Scikit Learn](https://scikit-learn.org/): Default framework
- [Vowpal Wabbit](https://vowpalwabbit.org/)
- [Tensorflow](https://www.tensorflow.org/)

The idea behind this wrapper is to give access to those OL algorithms with minimal configuration while 
providing the functionality related to using ML models (such as feature and class encoding, feature scaling, etc.) and integration capabilities with different AI platforms suck as Kubeflow or Seldon Core.

First thing to consider when instantiating the class is to provide the backend AI engine. For that there is an enumerator defined as:

```
from src.OnlineLearning import BackendType

class BackendType(Enum):
    TENSORFLOW = 'tensorflow'
    SKLEARN = 'sklearn'
    VOWPAL_WABBIT = 'vowpal_wabbit'
```

In addition to choosing one of the mentioned backends, the OL SDK supports regression and classification tasks through different configuration options. 

> Notice that the framework supports multiclass classification but not multivariate classification, neither multivariate regression. This means that outputs can only be one value per sample.

### Regression

Regression is the default option when instantiating the OL class, so you will just need to do (for the VOWPAL_WABBIT backend):

```
from src.OnlineLearning import BackendType, OnlineLearning

ol = OnlineLearning(BackendType.VOWPAL_WABBIT)
```

For regression tasks, features are automatically passed through an [StandardScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html), or a [MinMaxScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) if you are using the TENSORFLOW backend. You can override this pre-processing step by setting the parameter `auto_process_data` to `False` (it defaults to `True`). 

In addition, if your features also contain `categorical` values, those are automatically passed through a [FeatureHasher](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.FeatureHasher.html). 


### Classification

In classification mode, the OL SDK needs a few more initialization parameters to configure properly all the sub-components. Initialization with VOWPAL_WABBIT backend is like:

```
from src.OnlineLearning import BackendType, OnlineLearning

ol = OnlineLearning(BackendType.VOWPAL_WABBIT, inference_mode='classification', 
        classes = ('dog', 'cat'))
```

The classes are passed automatically through a [LabelEncoder](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html), and re-converted back when calling for predictions. 

Just as the regression task, features are passed through the scalers (either StandardScaler or MinMaxScaler) and categorical features are passed through the FeatureHasher. 

## Example of usage

Once the initialization is done, there are two main points of usage of the SDK:

* `Update`: Receives input and output data and updates the OL model accordingly.
* `Predict`: Receives input data and returns the expected prediction according to that data.

You would just need to do:

```
ol.update(input_data, output_data)
# or
ol.predict(input_data)
```

The input and output data are expected to be a `list of values`; or either a `python dictionary` or a `json-like object` in (feature, value) pairs. For example:

```
x = [15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 9.0, 8.0, 11.0, 12.0]
y = [15]

ol.update(x, y)
```

```
x = {'length': 15.0, 'height': 13.0, 'weight': 10.0, 'price': 7.0, 'color': 'red'}

ol.predict(x)
```

Although these methods support _batches_ of data, the way Online Learning algorithms work is usually processing _one sample at a time_ and updating the model with every sample. 

## Advanced configuration

In addition to the parameters explained above, the OnlineLearning class initialization accepts a `**kwargs` parameter, forwarding these **named parameters** to the selected backend framework. They include, for example, the learning rate, the dropout ratio, regularization parameters, etc. However, you must be aware that **every backend has its own parameter names**, so only use them if you are familiar with the backend framework.


### Scikit-Learn backend

The SKLearn backend also accepts a parameter named `algorithm` for the selection of the inner Online Learning algorithm. Remember that the OnlineLearning class accepts a `**kwargs` parameter, so the parameters that you would use to initialize any of these algorithms can be passed through the class. Default parameters are used if nothing is passed. There are three options for the algorithm selection:

* `PassiveAgressive` or `pa`: Stands for the Passive Aggressive [regressor](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.PassiveAggressiveRegressor.html) or [classifier](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.PassiveAggressiveClassifier.html?highlight=passive#sklearn.linear_model.PassiveAggressiveClassifier) according to `inference_mode` parameter. 
* `MultiLayerPerceptron` or `mlp`: Stands for the MultiLayer Perceptron [regressor](https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html) or [classifier](https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html) according to `inference_mode` parameter. 
* `StochasticGradientDescent` or `sgd`: Stands for the Stochastic Gradient Descent [regressor](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDRegressor.html) or [classifier](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html) according to `inference_mode` parameter. 

### Vowpal-Wabbit backend

The [Vowpal Wabbit](https://vowpalwabbit.org/) framework conforms a fast and powerful solution for online and active learning supported by an open community as well as by Microsoft Research. However, the learning curve for using this framework is quite steep. If you are not familiar with the framework, it is better to review the [basic tutorials](https://vowpalwabbit.org/tutorials.html) and [examples](https://github.com/VowpalWabbit/vowpal_wabbit/wiki/Examples). It may be also useful to read the [python documentation](https://vowpalwabbit.org/docs/vowpal_wabbit/python/8.9.0/).

A specific configuration is chosen to configure the framework towards pure online learning functionality (by default, the software supports other types of learning such as reinforcement learning) by using directly the python wrapper as shown [here](https://vowpal-wabbit.readthedocs.io/en/latest/) (although there exists a SKLearn interface). The classifier uses a strategy of One Against All and Hinge loss, similar to this [VW example](https://github.com/VowpalWabbit/vowpal_wabbit/wiki/One-Against-All-%28oaa%29-multi-class-example). For the regression task, default parameters are used. Remember that the backend has a `**kwargs` forwarding accepting any named parameters to the initialization of the framework. 


### Tensorflow backend

> **IMPORTANT: Currently this backend is on development as it requires more initial setup than the other two backends. Use the SKLearn or Vowpal Wabbit backends instead.**

Although deep learning models already support incremental learning via the traditional `fit` method (if the model is saved in memory, weights are updated every new call to fit), for a pure Online Learning implementation in [Tensorflow](https://www.tensorflow.org/) it is better to use the [`train_on_batch`](https://www.tensorflow.org/api_docs/python/tf/keras/Model#train_on_batch) method, as it "_runs a single gradient update on a single batch of data_". 

The disadvantage of Tensorflow, is that we need to predefine the model architecture before running the service. For this reason, two model architectures are provided initially, altough they **should be updated with more complex ones** in future iterations of this repository. They are defined in the [utils/ml/tensorflow_sdk.py](../utils/ml/tensorflow_sdk.py) file:

* `algorithm` = `dense`: 1 Dense Layer with 64 neurons plus the output layer.
* `algorithm` = `lstm`: 1 LSTM Layer with 64 neurons plus the output layer.

It is important to notice that the data shape for one option or the other is different; thus, when calling the update or predict methods, the data is reshaped automatically inside these functions. Default tensorflow parameters for this option are:

* `optimizer` = tf.keras.optimizers.RMSprop(lr=1e-3) 
* `loss` = tf.keras.losses.MeanAbsoluteError()
* `metrics` = [tf.keras.metrics.BinaryAccuracy(name='accuracy')]

