#   Copyright  2021 "". All rights reserved.

#   This file is part of "".

#   "" is free software: you can redistribute it and/or modify it under the terms of "", or
#   (at your option) any later version.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#   PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR
#   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#   See  LICENSE file for full license information  in the project root.

"""Online Learning module."""

from enum import Enum
import numpy as np
import pandas
from sklearn.preprocessing import StandardScaler, LabelEncoder #MinMaxScaler
from sklearn.feature_extraction import FeatureHasher

from utils.ml.pytorch_sdk import PytorchModel
from utils.ml.tensorflow_sdk import TensorflowModel
from utils.ml.sklearn_sdk import SkLearnModel
from utils.ml.vowpal_wb_sdk import VowpalWabbitModel

# from sklearn.feature_extraction import DictVectorizer

FEATUREHASHER_MAX_FEATURES = 100


class BackendType(Enum):
    """Backend Type Class."""

    TENSORFLOW = 'tensorflow'
    SKLEARN = 'sklearn'
    VOWPAL_WABBIT = 'vowpal_wabbit'
    PYTORCH = 'pytorch'


class OnlineLearning():
    """Online Learning class"""

    backend = None
    scaler_x = None
    scaler_y = None
    min_train_losses = None

    def __init__(self,*args, backend_type=BackendType.SKLEARN, inference_mode='regression',
                 classes=None, auto_process_data=True, performance_mode=True,
                 pretrained_model:object=None, **kwargs):
        """
        Constructor
            Parameters
                Inputs:
                    - backend_type
                    - inference_mode
                    - classes
                    - auto_process_data
                    - performace_mode
                    - pretrained_model
        """
        if backend_type == BackendType.PYTORCH:
            if pretrained_model is not None:
                print("Pretrained model")
                self.backend = PytorchModel(model=pretrained_model, *args,**kwargs)
            else:
                raise NotImplementedError("Pytorch backend is still on developments")

        elif backend_type == BackendType.TENSORFLOW:
            if pretrained_model is not None:
                print("Pretrained model")
                self.backend = TensorflowModel(model=pretrained_model, *args,**kwargs)
            else:
                raise NotImplementedError("Tensorflow backend is still on developments")

            # # TODO support multivariate regression
            # data_shape =(FEATUREHASHER_MAX_FEATURES, len(classes) if classes is not None else 1)

            # # from utils.ml.tensorflow_sdk import TensorflowModel
            # self.backend = TensorflowModel(*args, inference_mode=inference_mode,
            #                                data_shape=data_shape,
            #                                **kwargs)
            # if auto_process_data:
            #     self.scaler_x = MinMaxScaler(feature_range=(-1, 1))

            # if inference_mode == 'classification':
            #     assert classes is not None,"""In classification mode, you need to pass the
            #                                 classes on the initialization"""
            #     self.scaler_y = LabelEncoder()
            #     self.scaler_y.fit(classes)
            # else:
            #     self.scaler_y = MinMaxScaler(feature_range=(-1, 1))

            # if performance_mode:
            #     self.ft_transformer = FeatureHasher(
            #         n_features=FEATUREHASHER_MAX_FEATURES)
            # else:
            #     self.ft_transformer = None
            #     # self.ft_transformer = DictVectorizer()
        elif backend_type == BackendType.SKLEARN:
            # from utils.ml.sklearn_sdk import SkLearnModel
            self.ft_transformer = None
            # self.ft_transformer = DictVectorizer()
            if auto_process_data:
                self.scaler_x = StandardScaler()

            if inference_mode == 'classification':
                assert classes is not None, "In classification mode, you need to pass the classes"
                self.scaler_y = LabelEncoder()
                self.scaler_y.fit(classes)
                self.backend = SkLearnModel(
                    *args, inference_mode=inference_mode, classes=self.scaler_y.classes_, **kwargs)
            else:
                self.scaler_y = StandardScaler()
                self.backend = SkLearnModel(
                    *args, inference_mode=inference_mode, classes=None, **kwargs)

            if performance_mode:
                self.ft_transformer = FeatureHasher(
                    n_features=FEATUREHASHER_MAX_FEATURES)

        elif backend_type == BackendType.VOWPAL_WABBIT:
            # from utils.ml.vowpal_wb_sdk import VowpalWabbitModel
            self.scaler_y = StandardScaler()
            # self.backend = VowpalWabbitModel(
            #     *args, inference_mode=inference_mode, num_classes=None, **kwargs)
            self.backend = VowpalWabbitModel(*args, num_classes=None, **kwargs)

            self.ft_transformer = None
            # self.ft_transformer = DictVectorizer()

            if inference_mode == 'classification':
                assert classes is not None, "In classification mode, you need to pass the classes"
                self.scaler_y = LabelEncoder()
                self.scaler_y = self.scaler_y.fit(classes)
                self.backend = VowpalWabbitModel(
                    *args, inference_mode=inference_mode,
                    num_classes=len(self.scaler_y.classes_),
                    **kwargs)

            if performance_mode:
                self.ft_transformer = FeatureHasher(
                    n_features=FEATUREHASHER_MAX_FEATURES)

        else:
            raise Exception("Backend " + str(backend_type) +
                            " not recognized. Valid options are: " +
                            str(BackendType.TENSORFLOW) + ", " +
                            str(BackendType.SKLEARN) + ", or " +
                            str(BackendType.VOWPAL_WABBIT) + ".")

        self.metrics = {}
        self.metrics['updates_counter'] = 0
        self.metrics['predictions_counter'] = 0
        self.backend_type = backend_type

    def set_model(self, model):
        """
            Return the set model.

            Parameters
            ----------
            inputs : model to set in the backend.
            outputs: model set.
        """
        return self.backend.set_model(model)

    def get_model(self):
        """
            Return backend model.

            Parameters
            ----------
            inputs :
            outputs: backend model.
        """
        return self.backend.get_model()

    def update(self, inputs, outputs):
        """
            Perform an update on the model.

            Parameters
            ----------
            inputs : inputs to the model
            outputs: outputs to the model
        """
        self.metrics['updates_counter'] += 1

        # Prevent from errors in pandas when loading only 1 element
        if isinstance(inputs, float):
            inputs = [inputs]
        if isinstance(outputs, float):
            outputs = [outputs]
        print("Error 1")
        inputs = pandas.DataFrame.from_records(
            [inputs]).apply(pandas.to_numeric, errors='ignore')
        outputs = pandas.DataFrame.from_records([outputs])
        print("Error 2")
        try:
            if isinstance(self.scaler_y, LabelEncoder):
                # Use label encoder
                outputs_transformed = self.scaler_y.transform(
                    outputs.values.ravel())#reshape(1, -1))
            else:
                print("Error 3")
                # Use scaler
                outputs_transformed = self.scaler_y.fit_transform(
                    outputs.values.ravel())#reshape(1, -1))

            # Use FeatureHasher for hashing trick
            if self.ft_transformer:
                input_values = self.encode_data(inputs).values
            else:
                print("Error 4")
                input_values = inputs.values.reshape(1, -1)

            if self.backend_type == BackendType.VOWPAL_WABBIT:
                # Vowpal wabbit expects labels in form 1...num_classes
                outputs_transformed += 1
                # update = self.backend(input_values, outputs_transformed)
                self.backend(input_values, outputs_transformed)
            else:
                # Use Standard Scaler with online learning setting
                if self.scaler_x:
                    self.scaler_x = self.scaler_x.partial_fit(input_values)
                    inputs_transformed = self.scaler_x.transform(input_values)
                else:
                    inputs_transformed = input_values

                # update = self.backend(inputs_transformed, outputs_transformed)
                self.backend(inputs_transformed, outputs_transformed)
        except Exception as e:
            print("ERROR when computing model updates: " + str(e))
            return 1

        return 0

    def predict(self, input_data):
        """
            Return a prediction based on the input data.

            Parameters
            ----------
            input_data : input data in json format
        """
        self.metrics['predictions_counter'] += 1
        input_data = pandas.DataFrame.from_records([input_data])
        try:
            # Use FeatureHasher for hashing trick
            if self.ft_transformer:
                input_values = self.encode_data(input_data).values
            else:
                input_values = input_data.values.ravel()#reshape(1, -1)

            if self.backend_type == BackendType.VOWPAL_WABBIT:
                predictions = self.backend(input_values)
            else:
                # Use Standard Scaler with online learning setting
                if self.scaler_x:
                    inputs_transformed = self.scaler_x.transform(input_values)
                else:
                    inputs_transformed = input_values
                predictions = self.backend(inputs_transformed)

            if self.scaler_y:
                if self.backend_type == BackendType.VOWPAL_WABBIT:
                    # Vowpal wabbit expects labels in form 1...num_classes
                    predictions -= 1
                predictions = self.scaler_y.inverse_transform(
                    np.array([predictions])).tolist()

        except Exception as e:
            print("ERROR when computing model predictions: " + str(e))
            return None

        return predictions

    def info(self):
        """
            Return information about the deployment.
        """
        return self.metrics

    def encode_data(self, dataframe_global: pandas.DataFrame, dtypes='object', dropna=True):
        """ Reads dataframe and selects the columns that are not numbers.
            Those columns are removed and put into a feature_hasher to convert them to numbers.
            They are put back to the dataframe.

        Args:
            dataframe_global (pd.DataFrame): Pandas DataFrame with the actual data
            dtypes (str, optional): Type of encoded data. Defaults to 'object'.
            dropna (bool, optional): Remove rows from DataFrame with NaN values. Defaults to True.

        Returns:
            pandas.DataFrame: encoded data in a pandas DataFrame
        """
        dataframe_encode = dataframe_global

        non_numeric = dataframe_encode.select_dtypes(include=dtypes)
        numeric = dataframe_encode.select_dtypes(exclude=dtypes).reset_index()

        if "index" in numeric:
            numeric = numeric.drop(columns=["index"])

        if not non_numeric.empty:
            non_numeric_encoded = self.ft_transformer.fit_transform(
                non_numeric.to_dict(orient='records'))

            non_numeric_encoded = pandas.DataFrame.from_records(
                non_numeric_encoded.toarray())

            dataframe_encoded = pandas.concat(
                [numeric, non_numeric_encoded], axis=1)

            if dropna:
                dataframe_encoded = dataframe_encoded.dropna()

            return dataframe_encoded

        return dataframe_global
        # else:
        #     print("\tNo categorical data found with dtypes: " + str(dtypes))
        #     return dataframe_global

    def update_v2(self, inputs, outputs):
        """
        Perform an update on the model.

            Parameters
                Inputs:
                    inputs : inputs to the model
                    outputs: outputs to the model
                Outputs:
                    Boolean flag indicating if the model should be updated in MinIO
        """
        self.metrics['updates_counter'] += 1
        try:
            train_losses = self.backend.update_model(inputs, outputs)
            print(f"Min training losses: {self.min_train_losses}")
            print(f"Training losses: {train_losses}")
            if self.min_train_losses is None:
                self.min_train_losses = train_losses
                return True
            if self.min_train_losses is not None:
                if train_losses < self.min_train_losses:
                    self.min_train_losses = train_losses
                    return True
        except Exception as e:
            print("ERROR when computing model updates: " + str(e))
            return 1
        return False

    def predict_v2(self, inputs):
        """
        Perform an inference.

            Parameters
            ----------
            inputs : inputs to the model
        """
        self.metrics['predictions_counter'] += 1
        predictions = None
        try:
            predictions = self.backend.predict(inputs)

        except Exception as e:
            print("ERROR when computing inferences: " + str(e))
        return predictions
