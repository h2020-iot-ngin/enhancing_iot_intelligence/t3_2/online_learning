# Online Learning SDK for IoT-NGIN

## Backend frameworks

The sdk encapsulates three different frameworks that offer Incremental Learning algorithms:

- [Vowpal Wabbit](https://vowpalwabbit.org/): fast, flexible, online, and active learning solution.
- [Scikit Learn](https://scikit-learn.org/): Simple and efficient tools for predictive data analysis.
- [Tensorflow](https://www.tensorflow.org/): An end-to-end open source machine learning platform. 

The SDK comes with a default configuration for each of the frameworks. However, this configuration can be adjusted. To know more about how to configure each framework and how to choose between different algorithms and parameters, refer to the [Online Learning Readme file](./src/README.md). Some of those parameters can be directly configured upon launch as explained in the Serving section. 

## Serving as a REST API

Use [`serving/create_onlinelearning_service.py`](./serving/create_online_learning.py) script to launch a web server with the Online Learning module running as an HTTP REST API. Parameters for this script are:

- "--app_port": Default='5100'. Port where the app will communicate. 
- "--backend" Default='vowpal_wabbit'. Online learning backend framework. Options: tensorflow, sklearn, vowpal_wabbit.
- "--inference_mode": Default='regression'. Whether it is a regression or classification task.
- "--classes": Default='None'. List of classes for classification tasks. Examples on format: "('class_1', 'class_2')" or "(1,0)". __Remember to use `"` before and after the parenthesis__.

Example of launch:

```
$ python3 create_onlinelearning_service.py --backend sklearn --inference_mode classification --classes "('cat', 'dog')"
```

The service opens up two endpoints:

* `/update`: via `POST` method, it receives a `JSON` object which __must__ contain two elements: `inputs` and `outputs`, which can be either lists or named values.
* `/predict`: via `GET` method, it receives a `JSON` object which can be either a list of elements or named  feature-value pairs.

> Notice that the framework supports multiclass classification but not multivariate classification, neither multivariate regression. This means that outputs can only be one value per sample.

### Examples of usage:

The `/update` endpoint can be used as:

```
curl --location --request POST 'http://localhost:5100/update' \
--header 'Content-Type: application/json' \
--data-raw '{
    "input": {
        "a1": 1,
        "a2": 2,
        "a3": "tall"
    },
    "output": {
        "o1": "cat"
    }
}'
```

```
curl --location --request POST 'http://localhost:5100/update' \
--header 'Content-Type: application/json' \
--data-raw '{
    "input": [15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 9.0, 8.0, 11.0, 12.0],
    "output": [15.0]
}'
```

The `/predict` endpoint can be used as:

```
curl --location --request GET 'http://localhost:5100/predict' \
--header 'Content-Type: application/json' \
--data-raw '{
    "a1": 2,
    "a2": 1,
    "a3": "short"
}'
```

```
curl --location --request GET 'http://localhost:5100/predict' \
--header 'Content-Type: application/json' \
--data-raw '[15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 9.0, 8.0, 11.0, 12.0]'
```

## Stream Data Connectors

The SDK also provides the chance of getting the data using MQTT or Kafka. Unlike previous approach, there is no endpoint, since the online learning service receives the data automatically and makes updates or predictions depending on the topic presented by the messages.

The input arguments are the same as REST API service approach, and the following are added as well:

- "--connector_type": Default='mqtt'. Type of the data stream connector. Options: mqtt and kafka.
- "--broker_address": Default='127.0.0.1'. MQTT/Kafka broker IP address.
- "--broker_port_mqtt": Default=1883. MQTT broker port.
- "--broker_port_kafka": Default=19092. Kafka broker port.
- "--topic_to_update": Default='to_update'. Topic for sending data in order to update the model.
- "--topic_to_update": Default='to_update'. Topic for sending data in order to update the model.
- "--topic_to_predict": Default='to_predict'. Topic for sending data in order to get predictions from the model.
- "--predictions": Default='predictions'. Topic where the model sends the predictions obtained from'to_predict' topic data.


Example of launch: 

Since brokers are required to use MQTT and Kafka, these services can only be started using docker through [`docker-compose`](./docker-compose.yml) file.

```
$ docker-compose up
```

The online service is launched setting all input arguments by default and using kafka broker. To change any input argument is possible in __command__ field of __online_learning__ service in [`docker-compose`](./docker-compose.yml)
> Notice that when using docker, the broker address is the hostname set in the docker-compose file. The broker address must be `mosquitto_` for using MQTT. Also the `connector_type` argument must be set to "mqtt"

### Example of usage

At least 2 Kafka/MQTT agents are required. One agent must be a publisher to send the data to the model and the other a consumer to receive the predictions made by the model.

We provide a little service to create a publisher and a consumer Kafka/MQTT [`data_service`](./serving/data_service.py) with 2 endpoints: _/publish_ and _/consume_
* `/publish`: via `POST` method, it receives a `JSON` object which __must__ contain two elements: `topic` and `message`. `message` __must__ contain 
 `inputs` and `outputs` when update the model or just `inputs` when predictions are required.
* `/consume`: via `POST` method, it receives nothing and returns the `inputs` and the `predictions`


The input arguments are:

- "--app_port": Default=5101. Application port.
- "--connector_type": Default='kafka'. Type of the data stream connector. Options: mqtt and kafka.
- "--broker_address": Default='127.0.0.1'. MQTT/Kafka broker IP address.
- "--broker_port": Default=19092. Broker port.
- "--topic": Default='test'. Topic to subscribe when consumer mode.


Example of launch [`data_service`](./serving/data_service.py)


```
$ python3 data_service.py --topic predictions
```

Once the data service is launched, is easy to send data to the online learning service.


First we want to update the model, therefore `to_update` topic is used in order to send inputs and outputs:

```
curl --location --request POST 'http://localhost:5101/publish' \
--header 'Content-Type: application/json' \
--data-raw '{
            "topic":"to_update",
            "message": {
                        "input": [15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 5.0, 8.0, 9.0,11],
                        "output":["dog"]
                        }
            }'
```

Once the model is updated, it is possible to ask for a prediction sending data to `to_predict` topic:

```
curl --location --request POST 'http://localhost:5101/publish' \
--header 'Content-Type: application/json' \
--data-raw '{
            "topic":"to_predict",
            "message": 
                    {
                        "input": [15.0, 13.0, 10.0, 7.0, 9.0, 6.0, 5.0, 8.0, 9.0,11]
                    }
            }'
```

Automatically, once the online learning model performs the predictions, it sends them to `predictions` topic. The agent can consume that message through:

```
curl --location --request GET 'http://localhost:5101/consume'
```